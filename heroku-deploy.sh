#!/usr/bin/env bash

DEPLOY_PATH=heroku
HEROKU_NAME="dev-test-sis"

rm -rf $DEPLOY_PATH
mkdir -p $DEPLOY_PATH
rsync -av . $DEPLOY_PATH --exclude 'package-lock.json' --exclude node_modules

cd $DEPLOY_PATH
git init
heroku git:remote -a $HEROKU_NAME
git pull HEAD:master --force
git add .
git commit -m "deploy"
git push heroku HEAD:master --force


cd -
rm -rf $DEPLOY_PATH

echo "Deployment complete 🚀"