import { createContext, useContext } from "react";

const GlobalContext = createContext();

const useGlobalStore = () => {
  return useContext(GlobalContext);
};

export { GlobalContext, useGlobalStore };
