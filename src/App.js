import React, { useState } from "react";
import { Route, Switch, Redirect, BrowserRouter } from "react-router-dom";
import { GlobalContext } from "./context/global";
import PrivateRoute from "./components/PrivateRoute";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import routes from "./config/Routes";

import "./App.css";
import { Alert, AlertTitle } from "@material-ui/lab";

const App = () => {
  const [alertMessage, setAlertMessage] = useState({
    visible: false,
    title: "",
    message: "",
    variant: "",
  });

  const clearMessages = () => {
    setAlertMessage({});
  };

  const showMessage = (variant, title, message) => {
    window.scroll(0, 0);
    setAlertMessage({
      visible: true,
      title: title,
      message: message,
      variant: variant,
    });
  };

  return (
    <GlobalContext.Provider value={{ showMessage, clearMessages }}>
      {alertMessage.visible && (
        <Alert
          variant="filled"
          severity={alertMessage.variant}
          onClose={() => setAlertMessage({})}
          className="alert"
        >
          <AlertTitle>{alertMessage.title}</AlertTitle>
          {alertMessage.message}
        </Alert>
      )}

      <BrowserRouter>
        <Switch>
          <Route path="/login" component={Login} />
          <Route path="/logout" component={Logout} />
          {routes.map((r, i) => (
            <PrivateRoute
              path={r.path}
              component={r.component}
              key={`r-${i}`}
            />
          ))}
          <Redirect to="/trees" />
        </Switch>
      </BrowserRouter>
    </GlobalContext.Provider>
  );
};

export default App;
