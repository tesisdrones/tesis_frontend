export default {
  primary: "#40c293",
  white: "#FFFFFF",
  transparent: "transparent",
  disabled: "lightgray",
  green: "rgb(64, 194, 147)",
  powerGreen: "rgb(102, 253, 151",
  error: "red"
};
