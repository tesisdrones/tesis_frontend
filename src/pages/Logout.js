import React from "react";
import { useGlobalStore } from "../context/global";
import { Redirect } from "react-router-dom";
import { setUser } from "../config/Helpers";

const Logout = () => {
  setUser({});
  return <Redirect to="/login" />;
};

export default Logout;
