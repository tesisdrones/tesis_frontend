import { Grid } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { ScreenComponent } from "../components";
import Map from "../components/Map/Map";
import MapSwitchCard from "../components/Map/MapSwitchCard";
import MapScoringCard from "../components/Map/MapScoringCard";
import Spinner from "../components/Wrappers/Spinner";

import { getAllTrees } from "../config/ApiClient";
import { cutText, getLastItem, reloadPage } from "../config/Helpers";
import Modal from "../components/Wrappers/Modal";
import ImageScoring from "../components/ImageScoring/ImageScoring";
import Filter from "../components/Filter/Filter";
import Card from "../components/Card";
import TreeTable from "../components/TreeTable/TreeTable";
import moment from "moment";
import { defaultDateFormat } from "../config/AppConfig";
import { useGlobalStore } from "../context/global";
import { makeStyles } from "@material-ui/core/styles";

import TraceabilityDetails from "../components/ImageScoring/TraceabilityDetails";
import Chart from "react-apexcharts";

import _ from "lodash";

const TreesScreen = () => {
  const { showMessage } = useGlobalStore();
  const [treesData, setTreesData] = useState();
  const [loadingTrees, setLoadingTrees] = useState(true);
  const [treeInfo, setTreeInfo] = useState();
  const [listViewEnabled, setListViewEnabled] = useState(false);
  const [reQualifImage, setReQualifImage] = useState();
  const [treeScoresAmounts, setTreeScoresAmounts] = useState();
  const [chartData, setChartData] = useState();

  const [showImage, setShowImage] = useState();
  const [traceabilityToShow, setTraceabilityToShow] = useState();
  const [showImageDetails, setShowImageDetails] = useState();
  const [traceabilityModal, setTraceabilityModal] = useState(false);

  const useStyles = makeStyles((theme) => ({
    treeScreen: {
      minWidth: 150,
      minHeight: 830,
      maxHeight: 830,
    },
    componentRadius: {
      borderRadius: "10px",
      marginBottom: "10px",
    },
    mapComponent: {
      minHeight: '90vh',
      maxHeight: '90vh',
    },
    switchCard: {
      border: "1px solid rgb(64, 194, 147)",
      borderRadius: "10px",
      background: "white",
      color: "rgb(64, 194, 147)",
      fontWeight: "bold",
    },
    title: {
      fontWeight: "bold",
    },
  }));

  const classes = useStyles();

  useEffect(() => {
    fetchTrees();
  }, []);

  useEffect(() => {
    updateChartData();
  }, [treeScoresAmounts]);

  const fetchTrees = async () => {
    const treesResponse = await getAllTrees();
    if (treesResponse.ok) {
      const newData = treesResponse.response.map((tree) => {
        const lastImg = tree.images.length
          ? getLastItem(tree.images).image
          : {};
        return {
          treeData: {
            tree,
            ...lastImg,
            lastImageCreationDate: lastImg ? lastImg.creationDate : null,
          },
          latitude: tree.position.coordinates[0],
          longitude: tree.position.coordinates[1],
          score: lastImg ? lastImg.score : null,
        };
      });
      setTreesData(newData);
      updateChartData(newData);
      let scoresAmounts = newData.map((tree) => tree.treeData.tree.lastScore);
      let scoresBins = _.countBy(scoresAmounts);
      setTreeScoresAmounts({ scoresBins });
    } else {
      showMessage("Error al obtener el listado de árboles");
    }
    setLoadingTrees(false);
  };

  const updateChartData = () => {
    if (treeScoresAmounts) {
      const chartData = {
        series: [
          treeScoresAmounts.scoresBins[null] || 0,
          treeScoresAmounts.scoresBins[1] || 0,
          treeScoresAmounts.scoresBins[2] || 0,
          treeScoresAmounts.scoresBins[3] || 0,
          treeScoresAmounts.scoresBins[4] || 0,
          treeScoresAmounts.scoresBins[5] || 0,
        ],
        options: {
          labels: [
            "0 - Sin Calificar",
            "1 - Crítico",
            "2 - En Riesgo",
            "3 - Bueno",
            "4 - Muy Bueno",
            "5 - Óptimo",
          ],
          chart: {
            type: "donut",
            size: "10%",
            foreColor: "black",
          },
          plotOptions: {
            pie: {
              donut: {
                size: "65%",
              },
            },
          },
          colors: [
            "#cfd9e0",
            "#f09c26",
            "#ffe599",
            "#40c293",
            "#12e5af",
            "#66fd97",
          ],
          border: "#cfd9e0",
          dataLabels: {
            enabled: false,
            styles: {
              colors: [
                "#cfd9e0",
                "#f09c26",
                "#ffe599",
                "#40c293",
                "#12e5af",
                "#66fd97",
              ],
            },
          },
          fill: {
            colors: [
              "#cfd9e0",
              "#f09c26",
              "#ffe599",
              "#40c293",
              "#12e5af",
              "#66fd97",
            ],
          },
          legend: {
            color: [
              "#cfd9e0",
              "#f09c26",
              "#ffe599",
              "#40c293",
              "#12e5af",
              "#66fd97",
            ],
            useSeriesColors: true,
            show: true,
            position: "bottom",
          },
          responsive: [
            {
              breakpoint: 480,
              options: {
                chart: {
                  width: 200,
                },
                legend: {
                  display: "false",
                  position: "bottom",
                },
              },
            },
          ],
        },
      };
      setChartData(chartData);
    }
  };

  const toggleSwitch = () => {
    setListViewEnabled(!listViewEnabled);
  };

  const onSetTree = (tree) => {
    setTreeInfo(tree);
  };

  const onScore = () => {
    reloadPage();
  };

  const onReset = () => {
    reloadPage();
  };

  const onSetFiltered = (filteredData) => {
    setTreesData(filteredData);
  };

  const onShowTraceabilityHandler = () => {
    setTraceabilityToShow({
      ...treeInfo.tree,
      images: treeInfo.tree.images.reverse(),
    });
    setTraceabilityModal(true);
  };

  return (
    <ScreenComponent>
      {loadingTrees && (
        <Spinner
          message="CARGANDO DATOS..."
          containerStyles={{ marginTop: "30vh" }}
        />
      )}
      {!loadingTrees && (
        <Grid container spacing={2} className={classes.treeScreen}>
          {/* Filtros de búsqueda */}
          <Grid
            item
            xl={2}
            lg={3}
            md={3}
            xs={12}
            className={classes.componentRadius}
          >
            <Filter onSetFiltered={onSetFiltered} onReset={onReset} />
            {/* Chart */}
            <Card className={classes.componentRadius}>
              <p className={classes.title}>Estado de la Plantación</p>
              {!chartData && <Spinner />}
              {chartData && (
                <Chart
                  options={chartData.options}
                  series={chartData.series}
                  type="donut"
                />
              )}
            </Card>
          </Grid>

          <Grid
            item
            xl={8}
            lg={6}
            md={6}
            xs={12}
            className={classes.componentRadius}
            style={{ minHeight: 835, maxHeight: 835 }}
          >
            {loadingTrees && <Spinner />}
            {/* Vista de mapa */}
            {treesData && !listViewEnabled && (
              <Map className={classes.mapComponent} treesData={treesData} onSetTree={onSetTree} />
            )}
            {/* Vista de tabla */}
            {listViewEnabled && (
              <TreeTable
                treesData={treesData}
                setImageToShow={(image) => setShowImage(image)}
                setShowDetails={(tree) => onSetTree(tree)}
              />
            )}
          </Grid>

          {/* SIDEBAR */}
          <Grid
            item
            xl={2}
            lg={3}
            md={3}
            xs={12}
            className={classes.componentRadius}
          >
            {/* Switch de tipo de vista */}
            <MapSwitchCard
              toggleSwitch={toggleSwitch}
              listViewEnabled={listViewEnabled}
              className={classes.switchCard}
            />

            {/* Sidebar con detalles de árbol seleccionado */}
            {treeInfo ? (
              <>
                <MapScoringCard
                  imageFile={treeInfo.url}
                  onZoom={treeInfo.url && (() => setShowImage(treeInfo.url))}
                  score={treeInfo.score}
                  imageDate={
                    treeInfo.lastImageCreationDate &&
                    moment(treeInfo.lastImageCreationDate).format(
                      defaultDateFormat
                    )
                  }
                  tags={treeInfo.tree.tags || []}
                  details={cutText(treeInfo.details)}
                  onViewMoreDetails={
                    treeInfo.details &&
                    (() => setShowImageDetails(treeInfo.details))
                  }
                  onScore={treeInfo && (() => setReQualifImage(treeInfo))}
                  year={treeInfo.tree.year}
                  treeVariety={treeInfo.tree.variety}
                  onShowTraceability={onShowTraceabilityHandler}
                />
              </>
            ) : (
              <p>Seleccione un árbol para ver detalles</p>
            )}
          </Grid>
        </Grid>
      )}

      {/* Vista de imagen en modal */}
      <Modal open={showImage} onClose={() => setShowImage(false)} maxWidth="lg">
        <img src={showImage} width="100%" alt="img-preview" />
      </Modal>

      {/* Vista de trazabilidad en modal */}

      <TraceabilityDetails
        data={traceabilityToShow}
        onCloseModal={() => setTraceabilityModal(false)}
        showModal={traceabilityModal}
      />

      {/* Vista de detalles
       de la imagen */}
      <Modal
        title="Detalles del árbol"
        open={showImageDetails}
        onClose={() => setShowImageDetails(false)}
      >
        {showImageDetails}
      </Modal>

      {/* Modal de re-calificación */}
      {reQualifImage && (
        <ImageScoring
          show={reQualifImage}
          images={[reQualifImage]}
          onClose={() => setReQualifImage(false)}
          onUpdate={onScore}
        />
      )}
    </ScreenComponent>
  );
};

export default TreesScreen;
