import React, { useEffect, useState } from "react";
import { createTree, getAllTrees, getAllVarieties } from "../config/ApiClient";
import { useGlobalStore } from "../context/global";
import CustomButton from "../components/Wrappers/Button";
import Spinner from "../components/Wrappers/Spinner";
import CustomInput from "../components/Wrappers/Input";

import { ScreenComponent } from "../components";
import { getLastItem, reloadPage } from "../config/Helpers";
import Map from "../components/Map/Map";
import { Grid, MenuItem } from "@material-ui/core";
import DraggablePin from "../components/Map/DraggablePin";
import { aditionalTreeVarieties, defaultViewPort } from "../config/AppConfig";
import Colors from "../constants/Colors";
import Card from "../components/Card";
import { makeStyles } from "@material-ui/core";
import ContourLines from "../images/ContourLines.png";
import Tree from "../images/tree5.svg";

const useStyles = makeStyles((theme) => ({
  Title: {
    margin: "0px 5px 10px 5px",
    fontWeight: "bold",
    fontSize: "18px",
  },
  mapContainer: {
    overflow: "scroll",
    border: `2px solid rgb(102, 253, 151)`,
    borderRadius: "5px",
    height: "200px",
  },

  formItem: {
    width: "100%",
    margin: "7px 0",
  },
  imgWrapper: {
    overflow: "auto",
    height: "400px",
    background: "blue",
    position: "relative",
  },
  image: {
    cursor: "zoom-in",
  },
}));

const styles = {
  submitBtn: {
    margin: "20px auto",
    display: "block",
    width: "100%",
  },
  propInput: {
    margin: "5px 0",
    width: "100%",
  },
  helper: {
    color: Colors.primary,
  },
  input: {
    margin: "10px 0",
  },
};

const ResultValue = ({ title, result, actionTitle, onActionClick }) => (
  <div style={styles.resultContainer}>
    <span style={styles.title}>{title}</span>
    <span style={styles.result}>{result}</span>
    {onActionClick && (
      <small onClick={onActionClick} style={styles.action}>
        {actionTitle}
      </small>
    )}
  </div>
);

const TreesManagement = () => {
  const classes = useStyles();

  const { showMessage } = useGlobalStore();

  const [loadingTrees, setLoadingTrees] = useState(true);

  const [treesData, setTreesData] = useState();
  const [availableVarieties, setAvailableVarieties] = useState([]);

  const [newTree, setNewTree] = useState({
    row: undefined,
    column: undefined,
    variety: undefined,
    position: {
      lat: undefined,
      lon: undefined,
    },
    waypoint: {
      lat: undefined,
      lon: undefined,
    },
    height: 0,
  });

  const [marker, setMarker] = useState({
    latitude: defaultViewPort.latitude,
    longitude: defaultViewPort.longitude,
  });

  const [loadingSubmit, setLoadingSubmit] = useState(false);

  useEffect(() => {
    fetchTrees();
    fetchVarieties();
  }, []);

  const fetchTrees = async () => {
    const treesResponse = await getAllTrees();
    if (treesResponse.ok) {
      const collectionFormated = formatData(treesResponse.response);
      setTreesData(collectionFormated);
    } else {
      showMessage("Error al obtener el listado de árboles");
    }

    setLoadingTrees(false);
  };

  const formatData = (collectionData) => {
    const newData = collectionData.map((tree) => {
      return {
        latitude: tree.position.coordinates[0],
        longitude: tree.position.coordinates[1],
      };
    });
    return newData;
  };

  const fetchVarieties = async () => {
    const tagsResponse = await getAllVarieties();

    if (tagsResponse.ok) {
      setAvailableVarieties([
        ...tagsResponse.response,
        ...aditionalTreeVarieties,
      ]);
    } else {
      showMessage(
        "error",
        "Error inesperado obteniendo las variedades disponibles"
      );
    }
  };

  const onSubmit = async (event) => {
    event && event.preventDefault();
    setLoadingSubmit(true);

    const newTreeResponse = await createTree(newTree);

    if (newTreeResponse.ok) {
      setTreesData([
        ...treesData,
        {
          latitude: newTree.position.lat,
          longitude: newTree.position.lon,
        },
      ]);
      showMessage("success", "Árbol agregado correctamente");
      reloadPage();
    } else {
      showMessage(
        "error",
        "Error al agregar árbol, vuelva a intentarlo mas tarde"
      );
    }

    setLoadingSubmit(false);
  };

  const onSetTreeProp = (newProps) => {
    setNewTree({ ...newTree, ...newProps });
  };

  const onSetMarker = (newPosition) => {
    const { latitude, longitude } = newPosition;
    setMarker(newPosition);

    onSetTreeProp({ position: { lat: latitude, lon: longitude } });
  };

  return (
    <ScreenComponent>
      {loadingTrees && (
        <Spinner
          message="CARGANDO ÁRBOLES..."
          containerStyles={{ marginTop: "30vh" }}
        />
      )}

      {!loadingTrees && (
        <Grid container spacing={2}>
          <Grid item md={9} xs={12}>
            <Map
              treesData={treesData}
              //onMapClick={onMapClick}
              customPin={
                <DraggablePin marker={marker} setMarker={onSetMarker} />
              }
            />
            <span style={styles.helper}>
              Posiciona el{" "}
              <img alt="helper text" style={{ height: "20px" }} src={Tree} />{" "}
              para fijar las coordenadas del mismo.
            </span>
          </Grid>
          <Grid item md={3} xs={12}>
            <Card tooltip="Deslizate por el mapa de curvas de nivel para ver la altitud en la que se encuentra el arbol que deseas ingresar. las lineas rojas senalan saltos de un metro, mientas que las naranjas medio metro.">
              <p className={classes.Title}>Curvas de Nivel</p>
              <ResultValue
                result={
                  true ? (
                    <div className={classes.mapContainer}>
                      <img
                        src={ContourLines}
                        alt="imagen"
                        className={classes.image}
                        // onClick={onZoom}
                      />
                    </div>
                  ) : (
                    "s/e"
                  )
                }
              />
            </Card>
            <Card tooltip="Aquí debes ingresar todos los campos para poder dar de alta a un nuevo árbol en el sistema, debes arrastrar y soltar el icono del árbol para definir La latitud y la longitud del árbol. las coordenadas de waypoint deben ser extraidas del mapa georeferenciado confeccionado por el agrimensor, recuerda que es aquí donde se tomaran las fotos.">
              <div className={classes.Title}>Nuevo Árbol</div>
              <form onSubmit={onSubmit}>
                <CustomInput
                  type="text"
                  size="small"
                  label="Latitud Árbol"
                  value={newTree.position.lat || "-"}
                  disabled
                  required
                  style={styles.input}
                />
                <CustomInput
                  type="text"
                  size="small"
                  label="Longitud Árbol"
                  value={newTree.position.lon || "-"}
                  disabled
                  required
                  style={styles.input}
                />
                <CustomInput
                  type="number"
                  onChange={(e) =>
                    onSetTreeProp({
                      waypoint: {
                        lat: e.target.value,
                        lon: newTree.waypoint.lon,
                      },
                    })
                  }
                  inputProps={{ max: -55.363, min: -55.372, step: "0.000001" }}
                  size="small"
                  label="Latitud Waypoint"
                  required
                  style={styles.input}
                />
                <CustomInput
                  type="number"
                  inputProps={{ max: -34.363, min: -55.372, step: "0.000001" }}
                  onChange={(e) =>
                    onSetTreeProp({
                      waypoint: {
                        lat: newTree.waypoint.lat,
                        lon: e.target.value,
                      },
                    })
                  }
                  size="small"
                  label="Longitud Waypoint"
                  required
                  style={styles.input}
                />

                <CustomInput
                  type="number"
                  size="small"
                  label="Altitud"
                  inputProps={{ max: 64, min: 47, step: "0.1" }}
                  onChange={(e) =>
                    onSetTreeProp({
                      height: e.target.value,
                    })
                  }
                  required
                  style={styles.input}
                />

                <CustomInput
                  type="number"
                  inputProps={{ max: 99, min: 0 }}
                  onChange={(e) => onSetTreeProp({ row: e.target.value })}
                  size="small"
                  label="Número de fila"
                  required
                  style={styles.input}
                />

                <CustomInput
                  type="number"
                  inputProps={{ max: 99, min: 0 }}
                  onChange={(e) => onSetTreeProp({ column: e.target.value })}
                  size="small"
                  label="Número de columna"
                  required
                  style={styles.input}
                />

                <CustomInput
                  type="select"
                  onChange={(e) => onSetTreeProp({ variety: e.target.value })}
                  size="small"
                  label="Variedad"
                  required
                  style={styles.input}
                >
                  {availableVarieties.map((v, i) => (
                    <MenuItem key={i} value={v}>
                      {v}
                    </MenuItem>
                  ))}
                </CustomInput>

                <CustomButton
                  size="large"
                  type="submit"
                  variant="contained"
                  isloading={loadingSubmit}
                  style={styles.input}
                >
                  AGREGAR ÁRBOL
                </CustomButton>
              </form>
            </Card>
          </Grid>
        </Grid>
      )}
    </ScreenComponent>
  );
};

export default TreesManagement;
