import React, { useEffect, useState } from "react";
import { Redirect } from "react-router-dom";
import CustomButton from "../components/Wrappers/Button";
import { ReactComponent as Logo } from "../images/logoBlack.svg";

import colors from "../constants/Colors";
import CustomInput from "../components/Wrappers/Input";
import { forgotPassword, logIn } from "../config/ApiClient";
import { getUser, navigateTo, setUser } from "../config/Helpers";
import Colors from "../constants/Colors";
import Modal from "../components/Wrappers/Modal";

const styles = {
  form: {
    width: "100%",
  },
  title: {
    color: colors.primary,
    textAlign: "center",
  },
  contentCotainer: {
    padding: "30px",
    maxWidth: "500px",
    margin: "2vh auto",
  },
  select: {
    width: "100%",
    height: "3.5rem",
    borderRadius: "5px",
    marginTop: 16,
    marginBottom: 8,
  },
  loginBtn: {
    margin: "20px 0",
    height: 50,
    fontSize: 18,
  },
  input: {
    margin: "20px 0",
    width: "100%",
  },
  error: {
    color: "red",
    margin: "10px auto",
    textAlign: "center",
  },
  success: {
    color: "green",
    margin: "10px auto",
    textAlign: "center",
  },
};

const Login = () => {
  const [isLoggedIn, setLoggedIn] = useState(false);
  const [isError, setIsError] = useState(false);

  const [email, setEmail] = useState("");
  const [emailForgot, setEmailForgot] = useState("");
  const [password, setPassword] = useState("");
  const [forgotSuccess, setForgotSuccess] = useState(false);

  const [showForgotModal, setShowForgotModal] = useState(false);

  const [loadingSubmit, setLoadingSubmit] = useState(false);
  const [loadingFogrot, setLoadingForgot] = useState(false);

  useEffect(() => {
    const user = getUser();
    if (user && user.token) {
      setLoggedIn(true);
    }
  }, []);

  if (isLoggedIn) {
    return <Redirect to="/trees" />;
  }

  const handleSubmit = async (event) => {
    event && event.preventDefault();
    setIsError();

    setLoadingSubmit(true);

    const loginResponse = await logIn(email, password);

    if (loginResponse.ok) {
      setUser(loginResponse.response);
      navigateTo("/trees");
    } else {
      setIsError(true);
    }

    setLoadingSubmit(false);
  };

  const onSubmitForgot = async () => {
    setIsError(false);
    setForgotSuccess(false);

    setLoadingForgot(true);

    const forgotResponse = await forgotPassword(emailForgot);

    if (forgotResponse.ok) {
      setForgotSuccess(true);
    } else {
      setIsError(true);
    }

    setLoadingForgot(false);
  };

  return (
    <div style={styles.contentCotainer}>
      <Logo
        style={{
          display: "block",
          height: 180,
          margin: "-80px auto 20px",
        }}
      />
      <h1 style={styles.title}>Iniciar sesión</h1>
      <form onSubmit={handleSubmit}>
        <CustomInput
          type="text"
          required
          label="Email"
          value={email}
          error={isError}
          autocomplete
          onChange={(e) => {
            setEmail(e.target.value);
          }}
          style={styles.input}
          placeholder="Email"
        />
        <CustomInput
          required
          label="Contraseña"
          type="password"
          value={password}
          error={isError}
          onChange={(e) => {
            setPassword(e.target.value);
          }}
          placeholder="Contraseña"
        />
        <CustomButton
          spinnerSize={24}
          isloading={loadingSubmit}
          type="submit"
          fullWidth
          variant="contained"
          size="large"
          style={styles.loginBtn}
        >
          INGRESAR
        </CustomButton>
        <small
          style={{
            color: Colors.primary,
            margin: "10px auto 0",
            textAlign: "center",
            display: "block",
            cursor: "pointer",
          }}
          onClick={() => setShowForgotModal(true)}
        >
          ¿Olvidaste la contraseña?
        </small>
      </form>
      {isError && <div style={styles.error}>Datos inválidos</div>}
      {showForgotModal && (
        <Modal
          open={showForgotModal}
          title="Recuperar contraseña"
          disableDismiss
          onClose={() => setShowForgotModal(false)}
        >
          <CustomInput
            type="text"
            required
            label="Email"
            value={emailForgot}
            error={isError}
            autocomplete
            onChange={(e) => {
              setEmailForgot(e.target.value);
            }}
            style={styles.input}
            placeholder="Email"
          />
          <CustomButton
            spinnerSize={24}
            isloading={loadingFogrot}
            type="button"
            fullWidth
            variant="contained"
            size="large"
            style={styles.loginBtn}
            onClick={onSubmitForgot}
          >
            RECUPERAR
          </CustomButton>
          {isError && <div style={styles.error}>Datos inválidos</div>}
          {forgotSuccess && (
            <div style={styles.success}>
              Te enviamos un correo con tu nueva contraseña
            </div>
          )}
        </Modal>
      )}
    </div>
  );
};

export default Login;
