import React, { useState } from "react";
import ReactMapGL from "react-map-gl";
import Pins from "../../components/Map/Pins";
import { defaultViewPort, mapboxApiAccessToken } from "../../config/AppConfig";

const MissionPathView = ({ path, zoom }) => {
  const [viewport, setViewport] = useState({
    latitude: defaultViewPort.latitude,
    longitude: defaultViewPort.longitude,
    zoom: zoom || defaultViewPort.zoom,
  });
  return (
    <div style={{ width: 500, height: 500 }}>
      <ReactMapGL
        {...viewport}
        mapOptions
        mapboxApiAccessToken={mapboxApiAccessToken}
        mapStyle="mapbox://styles/mapbox/satellite-v9"
        width="100%"
        height="100%"
        onViewportChange={(viewport) => setViewport(viewport)}
      >
        <Pins data={path} onClick={() => null} />
      </ReactMapGL>
    </div>
  );
};

export default MissionPathView;
