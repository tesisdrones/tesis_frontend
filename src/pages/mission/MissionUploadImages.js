import React from "react";
import { UploadImages } from "../../components";

const MissionUploadImages = ({ missionId }) => {
  return (
    <div>
      <UploadImages missionId={missionId} />
    </div>
  );
};

export default MissionUploadImages;
