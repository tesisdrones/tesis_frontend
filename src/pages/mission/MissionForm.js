import { Grid, MenuItem } from "@material-ui/core";
import moment from "moment";
import React, { useEffect, useState } from "react";
import CustomButton from "../../components/Wrappers/Button";
import CustomInput from "../../components/Wrappers/Input";
import Spinner from "../../components/Wrappers/Spinner";
import { getAvaliablePaths } from "../../config/ApiClient";
import { defaultTreePaths } from "../../config/AppConfig";
import Colors from "../../constants/Colors";
import { useGlobalStore } from "../../context/global";

const styles = {
  formContainerStyles: {
    padding: 30,
    border: `1px solid ${Colors.primary}`,
    borderRadius: 10,
  },
  btnContainer: {
    margin: "20px 8px 0 auto",
  },
  btn: {
    margin: 5,
  },
};

const MissionForm = ({ onClose, onAdd, loadingSubmit }) => {
  const { showMessage } = useGlobalStore();

  const [name, setName] = useState();
  const [selectedPath, setSelectedPath] = useState();
  const [selectedDate, setSelectedDate] = useState(
    moment().format("yyyy-MM-DD")
  );

  const [loadingAvailableRows, setLoadingAvailableRows] = useState(false);

  const [availablePaths, setAvailablePaths] = useState([]);

  useEffect(() => {
    fetchAvailableRows();
  }, []);

  const fetchAvailableRows = async () => {
    setLoadingAvailableRows(true);

    const availableRowsResponse = await getAvaliablePaths();

    if (availableRowsResponse.ok) {
      const formatedPaths = availableRowsResponse.response.map((r) => ({
        label: `Fila ${r}`,
        value: r,
      }));
      setAvailablePaths([...defaultTreePaths, ...formatedPaths]);
    } else {
      setAvailablePaths(defaultTreePaths);
      showMessage("error", "Error al obtener las rutas de vuelo disponibles");
    }

    console.log(availableRowsResponse);

    setLoadingAvailableRows(false);
  };

  const onCancel = () => {
    onClose();
  };

  const onSubmit = (e) => {
    e.preventDefault();
    onAdd(name, selectedPath, selectedDate);
    onClose();
  };

  return (
    <form onSubmit={onSubmit} style={styles.formContainerStyles}>
      {loadingAvailableRows ? (
        <Spinner message="Cargando..." />
      ) : (
        <Grid container spacing={2}>
          <Grid item xs={6}>
            <CustomInput
              required
              variant="outlined"
              onChange={(e) => setName(e.target.value)}
              style={{ width: "100%" }}
              label="Nombre de mision"
            />
          </Grid>
          <Grid item xs={6}>
            <CustomInput
              type="select"
              label="Seleccione ruta de vuelo"
              size="medium"
              onChange={(e) => setSelectedPath(JSON.parse(e.target.value))}
              required
            >
              {availablePaths.map((p) => (
                <MenuItem key={0} value={JSON.stringify(p)}>
                  {p.label}
                </MenuItem>
              ))}
            </CustomInput>
          </Grid>
          <Grid item xs={6}>
            <CustomInput
              type="date"
              customLabel="Seleccione fecha de misión"
              size="medium"
              formatDate={(date) => moment(date).format("DD-MM-YYYY")}
              value={selectedDate}
              onChange={(e) => setSelectedDate(e.target.value)}
              required
            />
          </Grid>
          <div style={styles.btnContainer}>
            <CustomButton
              size="large"
              style={styles.btn}
              onClick={onCancel}
              variant="contained"
            >
              CANCELAR
            </CustomButton>
            <CustomButton
              type="submit"
              size="large"
              style={styles.btn}
              variant="contained"
              isloading={loadingSubmit || loadingAvailableRows}
            >
              CONTINUAR
            </CustomButton>
          </div>
        </Grid>
      )}
    </form>
  );
};

export default MissionForm;
