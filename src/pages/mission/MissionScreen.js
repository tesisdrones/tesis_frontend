import React, { useEffect, useState } from "react";
import moment from "moment";
import { Header, ScreenComponent } from "../../components";
import CustomButton from "../../components/Wrappers/Button";
import MissionCard from "./MissionCard";
import MissionForm from "./MissionForm";
import { defaultDateFormat, missionDateFormat } from "../../config/AppConfig";
import {
  addNewMission,
  getAllMissions,
  getPathByRow,
} from "../../config/ApiClient";
import Spinner from "../../components/Wrappers/Spinner";
import Modal from "../../components/Wrappers/Modal";
import MissionPathView from "./MissionPathView";
import MissionUploadImages from "./MissionUploadImages";
import Colors from "../../constants/Colors";
import { useGlobalStore } from "../../context/global";

const styles = {
  newMissionStyles: {
    margin: 20,
    float: "right",
  },
  formContainerStyles: {
    maxWidth: 600,
    margin: "0 auto",
    padding: 30,
  },
  noData: {
    textAlign: "center",
    color: Colors.primary,
  },
};

const MissionScreen = () => {
  const { showMessage } = useGlobalStore();

  const [missions, setMissions] = useState([]);
  const [showNewMission, setShowNewMission] = useState(false);
  const [showMissionPath, setShowMissionPath] = useState(false);
  const [showUploadImages, setShowUploadImages] = useState(false);

  const [missionLoading, setMissionLoading] = useState(false);
  const [newMissionLoading, setNewMissionLoading] = useState(false);

  useEffect(() => {
    fetchMissions();
  }, []);

  const fetchMissions = async () => {
    setMissionLoading(true);
    const missionsResponse = await getAllMissions();
    setMissions(missionsResponse.response.reverse());
    setMissionLoading(false);
  };

  const onAddMission = async (name, path, date) => {
    setNewMissionLoading(true);

    let addMissionResponse;

    if (path.value < 0) {
      addMissionResponse = await addNewMission(
        name,
        `Ruta de vuelo ${path.label}`,
        [],
        date
      );
    } else {
      const treePathResponse = await getPathByRow(path.value);
      addMissionResponse = await addNewMission(
        name,
        `Ruta de vuelo fila: ${path.value}`,
        treePathResponse.response.map((tp) => tp._id),
        date
      );
    }

    if (addMissionResponse.ok) {
      showMessage("success", "Misión agregada con éxito");
    } else {
      showMessage("error", "Error al agregar misión");
    }

    setNewMissionLoading(false);
    setMissionLoading(true);
    fetchMissions();
  };

  const onViewPath = (path) => {
    setShowMissionPath(path);
  };

  const onUploadImages = (id) => {
    setShowUploadImages(id);
  };

  const getMissionPath = (trees) => {
    return trees.length > 0
      ? trees.map((tree) => ({
          latitude: tree.position.coordinates[0],
          longitude: tree.position.coordinates[1],
        }))
      : [];
  };

  return (
    <ScreenComponent>
      {missionLoading && (
        <Spinner
          message="CARGANDO MISIONES..."
          containerStyles={{ marginTop: "30vh" }}
        />
      )}

      {!missionLoading && (
        <>
          {/* Botón nueva misión */}
          <CustomButton
            style={styles.newMissionStyles}
            onClick={() => setShowNewMission(true)}
            variant="contained"
          >
            Nueva Mission
          </CustomButton>

          {/* Form de nueva misión */}
          <div style={styles.formContainerStyles}>
            {showNewMission && (
              <MissionForm
                onClose={() => setShowNewMission(false)}
                onAdd={onAddMission}
                loadingSubmit={newMissionLoading}
              />
            )}

            {/* Listado de misiones */}
            {missions.map((m) => (
              <MissionCard
                id={m.id}
                path={getMissionPath(m.path.trees)}
                key={m.id}
                title={m.name}
                date={moment(m.date, missionDateFormat).format(
                  defaultDateFormat
                )}
                treeCount={m.path.trees.length}
                imagesCount={m.images.length}
                images={m.images}
                pathName={m.path.name}
                onViewPath={onViewPath}
                onUploadImages={onUploadImages}
              />
            ))}
            {!missionLoading && !showNewMission && missions.length === 0 && (
              <h5 style={styles.noData}>
                {" "}
                - UPS, PARECE QUE AÚN NO TIENES MISIONES -
              </h5>
            )}
          </div>

          {/* Modal de recorrido de misión */}
          {showMissionPath.length > 0 && (
            <Modal
              title="Ruta de vuelo"
              open={showMissionPath.length > 0}
              onClose={() => setShowMissionPath([])}
            >
              <MissionPathView path={showMissionPath} zoom={17} />
            </Modal>
          )}

          {/* Modal de Subida de imágenes */}
          {showUploadImages && (
            <Modal
              open={!!showUploadImages}
              onClose={() => {
                setShowUploadImages(false);
                fetchMissions();
              }}
            >
              <MissionUploadImages missionId={showUploadImages} />
            </Modal>
          )}
        </>
      )}
    </ScreenComponent>
  );
};

export default MissionScreen;
