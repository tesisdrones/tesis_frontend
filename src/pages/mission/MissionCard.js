import { Card, CardActions, CardContent, Grid } from "@material-ui/core";
import React, { useState } from "react";
import ImageScoring from "../../components/ImageScoring/ImageScoring";
import CustomButton from "../../components/Wrappers/Button";
import { getMission } from "../../config/ApiClient";
import Colors from "../../constants/Colors";
import colors from "../../constants/Colors";

const styles = {
  card: {
    backgroundColor: colors.primary,
    color: "white",
    margin: "40px 0",
  },
  actions: {
    display: "block",
    margin: "0 0 0 auto",
  },
  uploadBtn: {
    backgroundColor: "white",
    color: colors.primary,
    margin: 5,
  },
  status: {
    color: colors.white,
    fontWeight: "bold",
  },
};

const MissionCard = ({
  id,
  path,
  title,
  date,
  treeCount,
  imagesCount,
  pathName,
  onViewPath,
  onUploadImages,
}) => {
  const [missionImages, setMissionImages] = useState([]);
  const [loadingMission, setLoadingMission] = useState(false);

  const fetchMission = async (id) => {
    setLoadingMission(true);
    const missionResponse = await getMission(id);

    if (missionResponse.ok) {
      let imagesWithTrees = [];
      missionResponse.response.images.map((i) => {
        if (i.image) {
          const tree = missionResponse.response.path.trees.find(
            (t) => t._id === i.image.tree_id
          );
          tree &&
            imagesWithTrees.push({
              tree: tree,
              ...i.image,
            });
        }
      });
      setMissionImages(imagesWithTrees);
    }
    setLoadingMission(false);
  };

  return (
    <>
      {missionImages.length > 0 && (
        <ImageScoring
          images={missionImages}
          show={missionImages.length > 0}
          onClose={() => setMissionImages([])}
          mission={title}
        />
      )}

      <Card style={styles.card}>
        <CardContent>
          <Grid container spacing={2}>
            <Grid item md={4}>
              <small>TÍTULO</small>
              <h3>{title}</h3>
            </Grid>
            <Grid item md={4}>
              <small>FECHA</small>
              <h3>{date}</h3>
            </Grid>
            <Grid item md={4}>
              <small>RUTA DE VUELO</small>
              <h3>{pathName || "S/A"}</h3>
            </Grid>
          </Grid>
          <Grid container spacing={2}>
            <Grid item md={4}>
              <small>ÁRBOLES</small>
              <h3>{treeCount}</h3>
            </Grid>
            <Grid item md={4}>
              <small>IMÁGENES</small>
              <h3>{imagesCount}</h3>
            </Grid>
          </Grid>
        </CardContent>
        <CardActions>
          <div style={styles.actions}>
            <CustomButton
              size="small"
              style={styles.uploadBtn}
              variant="contained"
              onClick={() => onViewPath(path)}
            >
              VER RUTA DE VUELO
            </CustomButton>
            <CustomButton
              size="small"
              style={styles.uploadBtn}
              variant="contained"
              onClick={() => onUploadImages(id)}
            >
              SUBIR IMÁGENES
            </CustomButton>
            <CustomButton
              size="small"
              style={styles.uploadBtn}
              variant="contained"
              onClick={() => fetchMission(id)}
              isloading={loadingMission}
              spinnerColor={Colors.primary}
              disabled={!imagesCount}
            >
              CALIFICAR
            </CustomButton>
          </div>
        </CardActions>
      </Card>
    </>
  );
};

export default MissionCard;
