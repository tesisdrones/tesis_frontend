import TreesScreen from "../pages/TreesScreen";
import MissionScreen from "../pages/mission/MissionScreen";
import UpdatePasswordScreen from "../pages/UpdatePasswordScreen";
import TagsScreen from "../pages/TagsScreen";
import TreesManagement from "../pages/TreesManagement";

const routes = [
  {
    title: "PLANTACIÓN",
    path: "/trees",
    icon: "Terrain",
    component: TreesScreen,
  },
  {
    title: "MISIONES",
    path: "/mission",
    icon: "NearMe",
    component: MissionScreen,
  },
  {
    title: "TAGS",
    path: "/tags",
    icon: "LocalOffer",
    component: TagsScreen,
  },
  {
    title: "ÁRBOLES",
    path: "/trees-management",
    icon: "Eco",
    component: TreesManagement,
    // hideInMenu: true,
  },
  {
    title: "CAMBIAR CONTRASEÑA",
    path: "/password-update",
    component: UpdatePasswordScreen,
    hideInMenu: true,
  },
];

export default routes;
