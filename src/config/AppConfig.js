const defaultViewPort = {
  latitude: -34.753,
  longitude: -55.366361,
  zoom: 17,
  maxZoom: 18,
};

const androidAppDownloadLink =
  "https://tesis-mobile-app.s3.amazonaws.com/inventario_de_arboles.apk";

const mapboxApiAccessToken =
  "pk.eyJ1IjoiZ2VyZ2FyZGlvbCIsImEiOiJja2lxeHZsbHMwMWo2MnBwZm1ycHNod2E3In0.Ok61zq24q4GPYZ2we3OBKQ";

const defaultDateFormat = "DD/MM/YYYY";
const missionDateFormat = "YYYY-MM-DD";

const imageMaxFileSize = 25242880;
const imageQuality = 0.1;

const ScoringColorScale = [
  {
    score: 5,
    color: "green",
  },
  { score: 4, color: "lawngreen" },
  {
    score: 3,
    color: "orange",
  },
  {
    score: 2,
    color: "yellow",
  },
  {
    score: 1,
    color: "red",
  },
  {
    score: null,
    color: "white",
  },
];

const imageRates = [1, 2, 3, 4, 5];

const treePlantationStartYear = 2010;

const defaultTreePaths = [{ label: "Libre", value: -1 }];

const aditionalTreeVarieties = ["Nogal 1", "Nogal 2"];

export {
  ScoringColorScale,
  defaultViewPort,
  imageMaxFileSize,
  defaultDateFormat,
  missionDateFormat,
  imageQuality,
  imageRates,
  treePlantationStartYear,
  defaultTreePaths,
  aditionalTreeVarieties,
  mapboxApiAccessToken,
  androidAppDownloadLink,
};
