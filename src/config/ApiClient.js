import moment from "moment";
import { v4 } from "uuid";
import { missionDateFormat } from "./AppConfig";
import { getUser } from "./Helpers";

// prod - http://droneappbackend-env-2.eba-dtm63zdb.us-east-1.elasticbeanstalk.com

const apiUrl = "https://dev-tesis-sis-backend-dev.herokuapp.com";

//const apiUrl = "http://localhost:3001";

const headers = {
  "Content-Type": "application/json",
};

// --- START ENDPOINTS --->

const logIn = async (email, password) => {
  const data = {
    email,
    password,
  };
  const rawResponse = await fetch(`${apiUrl}/users/SignIn`, {
    method: "POST",
    headers: headers,
    body: JSON.stringify(data),
  });

  const requestResult = rawResponse.ok ? await rawResponse.json() : {};
  return { response: requestResult, ok: rawResponse.ok };
};

const forgotPassword = async (email) => {
  const data = {
    email,
  };
  const rawResponse = await fetch(`${apiUrl}/users/forgot`, {
    method: "POST",
    headers: headers,
    body: JSON.stringify(data),
  });

  const requestResult = rawResponse.ok ? await rawResponse.json() : {};
  return { response: requestResult, ok: rawResponse.ok };
};

const updatePassword = async (email, oldPassword, newPassword) => {
  const data = {
    email,
    oldPassword,
    newPassword,
  };
  const rawResponse = await fetch(`${apiUrl}/users/password`, {
    method: "PUT",
    headers: { ...headers, authorization: `Bearer ${getUser().token}` },
    body: JSON.stringify(data),
  });
  const requestResult = rawResponse.ok ? await rawResponse.json() : {};
  return { response: requestResult, ok: rawResponse.ok };
};

const uploadImage = async (missionId, imageFormData) => {
  const rawResponse = await fetch(`${apiUrl}/missions/${missionId}/images`, {
    method: "POST",
    headers: {
      authorization: `Bearer ${getUser().token}`,
    },
    body: imageFormData,
  });
  const requestResult = rawResponse.ok ? await rawResponse.json() : {};
  return { response: requestResult, ok: rawResponse.ok };
};

const addNewMission = async (missionName, pathName, pathTrees, date) => {
  const data = {
    id: v4(),
    name: missionName,
    pathName,
    pathTrees,
    date: moment(date).format(missionDateFormat),
  };
  const rawResponse = await fetch(`${apiUrl}/missions`, {
    method: "POST",
    headers: { ...headers, authorization: `Bearer ${getUser().token}` },
    body: JSON.stringify(data),
  });
  const requestResult = rawResponse.ok ? await rawResponse.json() : {};
  return { response: requestResult, ok: rawResponse.ok };
};

const getAllMissions = async () => {
  const rawResponse = await fetch(`${apiUrl}/missions`, {
    headers: { ...headers, authorization: `Bearer ${getUser().token}` },
  });
  const requestResult = rawResponse.ok ? await rawResponse.json() : {};
  return { response: requestResult, ok: rawResponse.ok };
};

const getMission = async (missionId) => {
  const rawResponse = await fetch(`${apiUrl}/missions/${missionId}`, {
    headers: { ...headers, authorization: `Bearer ${getUser().token}` },
  });
  const requestResult = rawResponse.ok ? await rawResponse.json() : {};
  return { response: requestResult, ok: rawResponse.ok };
};

const updateScoring = async (imageId, score, details, tags) => {
  const data = {
    tags: tags || [""],
    details,
  };
  const rawResponse = await fetch(
    `${apiUrl}/images/${imageId}/score/${score}`,
    {
      method: "PUT",
      headers: { ...headers, authorization: `Bearer ${getUser().token}` },
      body: JSON.stringify(data),
    }
  );
  const requestResult = rawResponse.ok ? await rawResponse.json() : {};
  return { response: requestResult, ok: rawResponse.ok };
};

const getAllTrees = async () => {
  const rawResponse = await fetch(`${apiUrl}/trees`, {
    headers: { ...headers, authorization: `Bearer ${getUser().token}` },
  });
  const requestResult = rawResponse.ok ? await rawResponse.json() : {};
  return { response: requestResult, ok: rawResponse.ok };
};

const getAvaliablePaths = async () => {
  const rawResponse = await fetch(`${apiUrl}/trees/path/rows`, {
    headers: { ...headers, authorization: `Bearer ${getUser().token}` },
  });
  const requestResult = rawResponse.ok ? await rawResponse.json() : {};
  return { response: requestResult, ok: rawResponse.ok };
};

const getPathByRow = async (rowNumber) => {
  const rawResponse = await fetch(`${apiUrl}/trees/row/${rowNumber}`, {
    headers: { ...headers, authorization: `Bearer ${getUser().token}` },
  });
  const requestResult = rawResponse.ok ? await rawResponse.json() : {};
  return { response: requestResult, ok: rawResponse.ok };
};

const applyFilters = async (filters) => {
  const rawResponse = await fetch(`${apiUrl}/trees/fields/filters`, {
    method: "POST",
    headers: { ...headers, authorization: `Bearer ${getUser().token}` },
    body: JSON.stringify({ filters: filters }),
  });
  const requestResult = rawResponse.ok ? await rawResponse.json() : {};
  return { response: requestResult, ok: rawResponse.ok };
};

const getAllTags = async () => {
  const rawResponse = await fetch(`${apiUrl}/tags`, {
    headers: { ...headers, authorization: `Bearer ${getUser().token}` },
  });
  const requestResult = rawResponse.ok ? await rawResponse.json() : {};
  return { response: requestResult, ok: rawResponse.ok };
};

const getAllVarieties = async () => {
  const rawResponse = await fetch(`${apiUrl}/trees/variety/all`, {
    headers: { ...headers, authorization: `Bearer ${getUser().token}` },
  });
  const requestResult = rawResponse.ok ? await rawResponse.json() : {};
  return { response: requestResult, ok: rawResponse.ok };
};

const createTag = async (tagName, tagColor) => {
  const rawResponse = await fetch(`${apiUrl}/tags`, {
    method: "POST",
    headers: { ...headers, authorization: `Bearer ${getUser().token}` },
    body: JSON.stringify({ name: tagName, color: tagColor }),
  });

  const requestResult = rawResponse.ok ? await rawResponse.json() : {};
  return { response: requestResult, ok: rawResponse.ok };
};

const deleteTag = async (tagName) => {
  const rawResponse = await fetch(`${apiUrl}/tags`, {
    method: "DELETE",
    headers: { ...headers, authorization: `Bearer ${getUser().token}` },
    body: JSON.stringify({ name: tagName }),
  });
  const requestResult = rawResponse.ok ? await rawResponse.json() : {};
  return { response: requestResult, ok: rawResponse.ok };
};

const createTree = async (newTree) => {
  const { row, column, variety, position, waypoint, height } = newTree;
  const data = {
    row,
    column,
    variety,
    position,
    waypoint,
    height,
  };
  const rawResponse = await fetch(`${apiUrl}/trees`, {
    method: "POST",
    headers: { ...headers, authorization: `Bearer ${getUser().token}` },
    body: JSON.stringify(data),
  });

  const requestResult = rawResponse.ok ? await rawResponse.json() : {};
  return { response: requestResult, ok: rawResponse.ok };
};

export {
  logIn,
  updatePassword,
  uploadImage,
  addNewMission,
  getAllMissions,
  getMission,
  updateScoring,
  getAllTrees,
  getPathByRow,
  applyFilters,
  getAllTags,
  createTag,
  deleteTag,
  getAllVarieties,
  createTree,
  getAvaliablePaths,
  forgotPassword,
};
