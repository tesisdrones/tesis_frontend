import moment from "moment";
import {
  imageQuality,
  ScoringColorScale,
  treePlantationStartYear,
} from "./AppConfig";

const setUser = (user) => {
  localStorage.setItem("wbsf-user", JSON.stringify(user));
};

const getUser = () => {
  const user = JSON.parse(localStorage.getItem("wbsf-user"));
  return user || "Usuario";
};

const getColorByScore = (score) => {
  return ScoringColorScale.find((sc) => sc.score === score).color;
};

const getDecimalCoordinates = (deg, min, sec, ref) => {
  const isNegative = ref === "S" || ref === "W";
  const decimalResult = deg + min / 60 + sec / 3600;
  return isNegative ? decimalResult * -1 : decimalResult;
};

const navigateTo = (url) => {
  window.location.href = url;
};

const minifyImg = (file) => {
  var img = new Image();
  img.src = URL.createObjectURL(file);
  return new Promise((resolve) => {
    img.onload = () => {
      var canvas = document.createElement("canvas");
      canvas.width = img.width;
      canvas.height = img.height;
      var ctx = canvas.getContext("2d");
      ctx.drawImage(img, 0, 0);
      canvas.toBlob((blob) => resolve(blob), "image/jpeg", imageQuality);
    };
  });
};

const blobToFile = (blobFile, fileName) => {
  return new File([blobFile], fileName, {
    lastModified: new Date().getTime(),
    type: blobFile.type,
  });
};

const cutText = (text, end) => {
  return text ? `...${text.substring(text.length - end, text.length)}` : "";
};

const getLastItem = (array) => {
  return array[array.length - 1];
};

const getYearsFromBegining = () => {
  const start = treePlantationStartYear;

  const rangeOfYears = (start, end) =>
    Array(end - start + 1)
      .fill(start)
      .map((year, index) => year + index);

  return rangeOfYears(start, moment().format("YYYY"));
};

const reloadPage = () => {
  window.location.reload();
};

const validatePassword = (password) => {
  const PASSWORD_REGEX =
    /^(?=.*[a-z])(?=.*\d)[A-Za-z\d[\]{};:.,=<>_+^#$@!%*?&]{8,30}$/;

  if (!PASSWORD_REGEX.test(password)) return false;

  return true;
};

export {
  setUser,
  getUser,
  getColorByScore,
  navigateTo,
  getDecimalCoordinates,
  minifyImg,
  blobToFile,
  cutText,
  getLastItem,
  getYearsFromBegining,
  reloadPage,
  validatePassword,
};
