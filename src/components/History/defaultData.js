const colHeaders = [
  "Identificador",
  "Fila",
  "Columna",
  "Latitud",
  "Longitud",
  "Variedad",
  "Año",
  "Último puntaje",
  "Última imagen",
];

const dataMock = [
  {
    id: 0,
    row: 3,
    col: 12,
    lat: "-32.41231413",
    long: "-23.41231213",
    variety: "Pecán Simple",
    year: 2020,
    lastScoringResult: 4,
    lastImage: "-",
  },
  {
    id: 1,
    row: 4,
    col: 12,
    lat: "-32.41232134",
    long: "-23.41244444",
    variety: "Pecán Simple",
    year: 2014,
    lastScoringResult: 2,
    lastImage: "-",
  },
  {
    id: 2,
    row: 5,
    col: 12,
    lat: "-32.412313512",
    long: "-24.55231213",
    variety: "Pecán Simple",
    year: 2012,
    lastScoringResult: 5,
    lastImage: "-",
  },
];

export { colHeaders, dataMock };
