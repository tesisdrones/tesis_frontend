import React from "react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { colHeaders, dataMock } from "./defaultData";

const styles = {
  table: {
    minWidth: 650,
  },
  noData: {
    color: "rgb(0, 151, 204)",
    margin: "20px",
  },
  colTitle: {
    color: "rgb(0, 151, 204)",
    textAlign: "center",
    fontWeight: "bold",
  },
};

const HistoryTable = () => {
  return (
    <>
      <TableContainer component={Paper}>
        <Table style={styles.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              {colHeaders.map((colName) => (
                <TableCell style={styles.colTitle}>{colName}</TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {dataMock ? (
              dataMock.map((tree) => (
                <TableRow key={tree.id}>
                  <TableCell align="center">{tree.id}</TableCell>
                  <TableCell align="center">{tree.row}</TableCell>
                  <TableCell align="center">{tree.col}</TableCell>
                  <TableCell align="center">{tree.lat}</TableCell>
                  <TableCell align="center">{tree.long}</TableCell>
                  <TableCell align="center">{tree.variety}</TableCell>
                  <TableCell align="center">{tree.year}</TableCell>
                  <TableCell align="center">{tree.lastScoringResult}</TableCell>
                  <TableCell align="center">
                    <a href={`${tree.lastImage}`}>VER IMAGEN</a>
                  </TableCell>
                </TableRow>
              ))
            ) : (
              <h3 style={styles.noData}>SIN DATOS</h3>
            )}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
};

export default HistoryTable;
