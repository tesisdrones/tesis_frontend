import React from "react";
import colors from "../constants/Colors";
import { makeStyles } from "@material-ui/core/styles";
import Eco from "@material-ui/icons/Eco";
import NearMe from "@material-ui/icons/NearMe";
import LocalOffer from "@material-ui/icons/LocalOffer";
import Terrain from "@material-ui/icons/Terrain";

import Tooltip from "@material-ui/core/Tooltip";

const styles = {
  selectedBtn: {
    backgroundColor: "white",
    color: colors.primary,
    border: 0,
    borderRadius: 15,
    fontSize: 18,
    cursor: "pointer",
    padding: "5px 15px",
    fontWeight: "bold",
  },
  unselectedBtn: {
    backgroundColor: colors.primary,
    color: "white",
    border: 0,
    fontSize: 18,
    cursor: "pointer",
    padding: "5px 15px",
    fontWeight: "bold",
  },
};

const useStyles = makeStyles((theme) => ({
  Tooltip: {
    margin: "5px",
  },
}));

const MenuOption = ({ title, onClick, selected, icon }) => {
  const classes = useStyles();

  const IconRender = () => {
    switch (icon) {
      case "Eco":
        return <Eco />;
      case "LocalOffer":
        return <LocalOffer />;
      case "NearMe":
        return <NearMe />;
      case "Terrain":
        return <Terrain />;
      default:
        break;
    }
  };

  const ToolTipRender = () => {
    switch (icon) {
      case "Eco":
        return (
          <Tooltip
            className={classes.Tooltip}
            title="En esta sección puedes ingresar nuevos árboles al sistema."
          >
            {IconRender()}
          </Tooltip>
        );
      case "LocalOffer":
        return (
          <Tooltip
            className={classes.Tooltip}
            title="Permite gestionar las etiquetas que se usarán para marcar los árboles."
          >
            {IconRender()}
          </Tooltip>
        );
      case "NearMe":
        return (
          <Tooltip
            className={classes.Tooltip}
            title="En esta sección puedes crear nuevas misiones y cargar la información recopilada en ellas."
          >
            {IconRender()}
          </Tooltip>
        );
      case "Terrain":
        return (
          <Tooltip
            className={classes.Tooltip}
            title="Puedes seleccionar y filtrar los árboles. una vez seleccionados puedes ver su información, calificarlos y agregarle notas o etiquetas."
          >
            {IconRender()}
          </Tooltip>
        );
      default:
        break;
    }
  };

  return (
    <div>
      <button
        type="button"
        onClick={onClick}
        style={selected ? styles.selectedBtn : styles.unselectedBtn}
      >
        {ToolTipRender()}
        {title}
      </button>
    </div>
  );
};

export default MenuOption;
