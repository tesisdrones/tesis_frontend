import React from "react";
import HelpOutline from "@material-ui/icons/HelpOutline";
import Tooltip from "@material-ui/core/Tooltip";
import Colors from "../constants/Colors";

const styles = {
  container: {
    backgroundColor: Colors.white,
    border: `1px solid ${Colors.green}`,
    borderRadius: "10px",
    padding: "10px 20px",
    color: Colors.primary,
    boxShadow: "0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)",
    marginBottom: "10px",
  },
  containerSecondary: {
    boxShadow: "0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)",
    padding: "10px 20px 0",
    marginBottom: 20,
    color: `${Colors.primary}`,
    borderRadius: "10px",
  },
  tooltip: {
    float: "right",
  },
};

const Card = ({ children, style, secondary, tooltip, tooltipStyles }) => {
  return (
    <div
      style={
        style
          ? { ...styles.container, ...style }
          : secondary
          ? styles.containerSecondary
          : styles.container
      }
    >
      {tooltip && (
        <Tooltip
          style={{ ...styles.tooltip, ...tooltipStyles }}
          title={tooltip}
        >
          <HelpOutline />
        </Tooltip>
      )}
      {children}
    </div>
  );
};

export default Card;
