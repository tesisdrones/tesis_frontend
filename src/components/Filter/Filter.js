import { MenuItem } from "@material-ui/core";
import { isEmpty } from "lodash";
import React, { useEffect, useState } from "react";
import { Card } from "..";
import {
  applyFilters,
  getAllTags,
  getAllVarieties,
} from "../../config/ApiClient";
import { imageRates } from "../../config/AppConfig";
import { getYearsFromBegining } from "../../config/Helpers";
import { useGlobalStore } from "../../context/global";
import CustomButton from "../Wrappers/Button";
import CustomInput from "../Wrappers/Input";
import { getLastItem } from "../../config/Helpers";

const styles = {
  container: {
    border: "1px solid"
  },
  sectionTitle: {
    fontSize: 18,
    color: "rgb(64, 194, 147)",
    fontWeight: "bold",
    marginLeft: "3px",
    display: "inline-flex",
  },
  btnContainer: {
    margin: "20px 0",
    width: "100%",
    display: "inline-block",
  },
  btn: {
    width: "100%",
    margin: "2px 0",
  },
  item: {
    width: "100%",
  },
  input: {
    margin: "5px 0",
  },
};

const Filter = ({ onSetFiltered, onReset }) => {
  const { showMessage, clearMessages } = useGlobalStore();

  const [availableTags, setAvailableTags] = useState([]);
  const [availableTagsObject, setAvailableTagsObject] = useState([]);

  const [availableVarieties, setAvailableVarieties] = useState([]);

  const [filter, setFilters] = useState({});

  const [loadingFilters, setLoadingFilters] = useState(false);

  useEffect(() => {
    fetchTags();
    fetchVarieties();
  }, []);

  const fetchTags = async () => {
    const tagsResponse = await getAllTags();

    if (tagsResponse.ok) {
      setAvailableTags(tagsResponse.response.map((t) => t.name));
      setAvailableTagsObject(tagsResponse.response.map((t) => t));
    } else {
      showMessage("error", "Error inesperado obteniendo tags disponibles");
    }
  };

  const fetchVarieties = async () => {
    const tagsResponse = await getAllVarieties();

    if (tagsResponse.ok) {
      setAvailableVarieties(tagsResponse.response);
    } else {
      showMessage(
        "error",
        "Error inesperado obteniendo las variedades disponibles"
      );
    }
  };

  const onSubmit = async () => {
    clearMessages();

    if (isEmpty(filter)) {
      showMessage(
        "error",
        "Debe seleccionar alguno de los criterios de búsqueda"
      );
      return;
    }

    setLoadingFilters(true);

    const filters = Object.keys(filter).map((k) => ({ [k]: filter[k] }));
    const selectedTags = filters.filter((e) => e["tags"])[0]?.tags;
    const tagsIds = availableTagsObject.filter(
      (element) => element.name === selectedTags
    );

    if (tagsIds.length > 0) {
      filters.find((e) => e.tags).tags = tagsIds[0]?._id;
    }
    const filtersResponse = await applyFilters(filters);

    if (filtersResponse.ok) {
      const newData = filtersResponse.response.map((tree) => {
        const lastImg = tree.images.length
          ? getLastItem(tree.images).image
          : {};
        return {
          treeData: { tree, ...lastImg },
          latitude: tree.position.coordinates[0],
          longitude: tree.position.coordinates[1],
          score: lastImg ? lastImg.score : null,
        };
      });
      onSetFiltered(newData);
    } else {
      showMessage("error", "Error inesperado filtrando los resultados");
    }

    setLoadingFilters(false);
  };

  return (
    <Card
      style={styles.container}
      secondary
      tooltip="Elije filtros para concentrarte en la categoria que buscas. Los resultados se mostraran en la tabla/mapa."
    >
      <p style={styles.sectionTitle}>Filtros</p>

      <CustomInput
        type="select"
        style={styles.input}
        onChange={(e) => setFilters({ ...filter, variety: e.target.value })}
        size="medium"
        label="Variedad"
      >
        {availableVarieties.map((v, i) => (
          <MenuItem key={i} value={v}>
            {v}
          </MenuItem>
        ))}
      </CustomInput>

      <CustomInput
        type="select"
        style={styles.input}
        onChange={(e) => setFilters({ ...filter, lastScore: e.target.value })}
        size="medium"
        label="Calificación"
      >
        {imageRates.map((v, i) => (
          <MenuItem key={i} value={v}>
            {v}
          </MenuItem>
        ))}
      </CustomInput>

      <CustomInput
        style={styles.input}
        type="select"
        onChange={(e) => setFilters({ ...filter, year: e.target.value })}
        size="medium"
        label="Año"
      >
        {getYearsFromBegining().map((v, i) => (
          <MenuItem key={i} value={v}>
            {v}
          </MenuItem>
        ))}
      </CustomInput>

      <CustomInput
        type="select"
        style={styles.input}
        onChange={(e) => setFilters({ ...filter, tags: e.target.value })}
        size="medium"
        label="Tag"
      >
        {availableTags.length > 0 &&
          availableTags.map((t, i) => (
            <MenuItem key={`tag-${i}`} value={t}>
              {t}
            </MenuItem>
          ))}
      </CustomInput>

      <div style={styles.btnContainer}>
        <CustomButton
          onClick={onSubmit}
          isloading={loadingFilters}
          style={styles.btn}
        >
          Buscar
        </CustomButton>
        <CustomButton onClick={onReset} style={styles.btn}>
          Limpiar
        </CustomButton>
      </div>
    </Card>
  );
};

export default Filter;
