import React, { useState } from "react";
import { updatePassword } from "../../config/ApiClient";
import { getUser, navigateTo } from "../../config/Helpers";
import Colors from "../../constants/Colors";
import { useGlobalStore } from "../../context/global";
import CustomButton from "../Wrappers/Button";
import CustomInput from "../Wrappers/Input";

const styles = {
  contentCotainer: {
    padding: "30px",
    maxWidth: 500,
    margin: "10vh auto",
  },
  input: {
    margin: "10px 0",
    width: "100%",
  },
  submitBtn: {
    width: "100%",
    height: 50,
    marginTop: 20,
  },
  title: {
    color: Colors.primary,
    textAlign: "center",
    marginBottom: 40,
  },
};

const UpdatePassword = () => {
  const { showMessage } = useGlobalStore();

  const [loadingSubmit, setLoadingSubmit] = useState();
  const [oldPassword, setOldPassword] = useState();
  const [newPassword, setNewPassword] = useState();
  const [newPasswordRepeat, setNewPasswordRepeat] = useState();

  const onSubmit = async (e) => {
    e && e.preventDefault();
    setLoadingSubmit(true);

    if (!oldPassword || !newPassword || !newPasswordRepeat) {
      showMessage("info", "Todos los campos son obligatorios");
      setLoadingSubmit(false);
      return;
    }

    if (newPassword !== newPasswordRepeat) {
      showMessage("info", "Las nuevas contraseñas deben coincidir");
      setLoadingSubmit(false);
      return;
    }

    const user = getUser();
    const updateResponse = await updatePassword(
      user.user.email,
      oldPassword,
      newPassword
    );

    if (updateResponse.ok) {
      showMessage("success", "Cambio de contraseña exitoso");
      setTimeout(() => {
        navigateTo("/logout");
      }, 2000);
    } else {
      showMessage(
        "error",
        "Error al cambiar la contraseña, intentelo mas tarde"
      );
    }

    setLoadingSubmit(false);
  };

  const passwordInvalid =
    newPassword && newPasswordRepeat && newPassword !== newPasswordRepeat;

  return (
    <div style={styles.contentCotainer}>
      <h3 style={styles.title}>Cambio de contraseña</h3>
      <form onSubmit={onSubmit}>
        <CustomInput
          style={styles.input}
          label="Contraseña actual"
          value={oldPassword}
          required
          onChange={(e) => setOldPassword(e.target.value)}
        />
        <CustomInput
          style={styles.input}
          label="Contraseña nueva"
          value={newPassword}
          required
          error={passwordInvalid}
          onChange={(e) => setNewPassword(e.target.value)}
        />
        <CustomInput
          style={styles.input}
          required
          label="Repite contraseña nueva"
          value={newPasswordRepeat}
          error={passwordInvalid}
          onChange={(e) => setNewPasswordRepeat(e.target.value)}
          helperText={
            passwordInvalid ? "La contraseña es inválida o no coincide" : ""
          }
        />
        <CustomButton
          size="large"
          type="submit"
          style={styles.submitBtn}
          variant="contained"
          onClick={onSubmit}
          isloading={loadingSubmit}
        >
          CAMBIAR CONTRASEÑA
        </CustomButton>
      </form>
    </div>
  );
};

export default UpdatePassword;
