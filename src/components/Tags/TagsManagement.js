import React, { useEffect, useState } from "react";
import {
  createTag,
  deleteTag,
  getAllTags,
  updatePassword,
} from "../../config/ApiClient";
import { getUser, navigateTo } from "../../config/Helpers";
import Colors from "../../constants/Colors";
import { useGlobalStore } from "../../context/global";
import CustomButton from "../Wrappers/Button";
import CustomInput from "../Wrappers/Input";
import TagCard from "../TagCard/TagCard";
import { TwitterPicker } from 'react-color';
import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles((theme) => ({
  colorPicker: {
    margin: "5px",
    padding: "15px",
    border: "1px solid lightgray",
    borderRadius: "5px",
    display: "flex",
    width: "fit-content !important"

  }
}))

const styles = {
  contentCotainer: {
    padding: "30px",
    maxWidth: 500,
    margin: "10vh auto",
  },
  input: {
    margin: "10px 0",
    width: "100%",
  },
  submitBtn: {
    width: "100%",
    height: 50,
    marginTop: 20,
  },
  title: {
    color: Colors.primary,
    textAlign: "center",
    marginBottom: 40,
  },
  tagsContainer: {
    marginBottom: 30,
  },
  noTags: {
    textAlign: "center",
    display: "block",
    margin: "0 auto",
    color: Colors.primary,
  },
};

const TagsManagement = () => {

  const classes = useStyles();
  const colors = ["#66fd97", "#40c293", "#009566", "#cfd9e0", "#f09c26", "#12e5af", "#FFE599", "#A56C48", "#F1D02D"]

  const { showMessage } = useGlobalStore();

  const [newTag, setNewTag] = useState();
  const [availableTags, setAvailableTags] = useState([]);
  const [loadingTags, setLoadingTags] = useState(false);
  const [loadingDeleteTag, setLoadingDeleteTag] = useState(false);
  const [loadingSubmit, setLoadingSubmit] = useState(false);
  const [tagColor, setTagColor] = useState();
  const [tagsChanged, setTagsChanged] = useState(true)

  useEffect(() => {
    fetchTags();
  }, [availableTags]);

  const fetchTags = async () => {
    if(tagsChanged){
      const tagsResponse = await getAllTags();
      if (tagsResponse.ok) {
        setAvailableTags(tagsResponse.response.map((t) => t ));
      } else {
        console.error(tagsResponse);
      }
      setTagsChanged(false)
    }
  };

  const onSubmit = async (event) => {
    event && event.preventDefault();
    setLoadingSubmit(true);

    const color = tagColor.hex
    const tagResponse = await createTag({newTag, color});

    if (tagResponse.ok) {
      setAvailableTags([...availableTags, tagResponse.response]);
      showMessage("success", "Tag agregada correctamente");
    } else {
      showMessage("error", "Error al agregar Tag, intentelo mas tarde");
    }
    setTagsChanged(true)
    setLoadingSubmit(false);
  };

  const onDeleteTag = async (tag) => {
    setLoadingDeleteTag(true);

    const deleteResponse = await deleteTag(tag);

    if (deleteResponse.ok) {
      setAvailableTags(availableTags.filter((t) => t.name !== tag.name));
      showMessage("success", "Tag eliminada correctamente");
    } else {
      showMessage("error", "Error al eliminar tag, intentelo mas tarde");
    }
    setTagsChanged(true)
    setLoadingDeleteTag(false);
  };


  return (
    <div style={styles.contentCotainer}>
      <h3 style={styles.title}>Tags</h3>

      <div style={styles.tagsContainer}>
        {availableTags.length > 0 ? (
          availableTags.map((tag) => (
            <TagCard name={tag.name} onDeleteTag={() => onDeleteTag(tag)} color={tag.color}/>
          ))
        ) : (
          <span style={styles.noTags}>- SIN TAGS -</span>
        )}
      </div>

      <form onSubmit={onSubmit}>
        <CustomInput
          type="text"
          required
          label="Nueva tag"
          value={newTag}
          onChange={(e) => setNewTag(e.target.value)}
          style={styles.input}
        />
        <TwitterPicker
          required
          // onSwatchHover={"onMouseOver"}
          triangle={"hide"}
          colors={colors}
          className={classes.colorPicker}
          value={tagColor} onChange={setTagColor}/>
        <CustomButton
          size="large"
          type="submit"
          style={styles.submitBtn}
          variant="contained"
          isloading={loadingSubmit}
        >
          AGREGAR NUEVA TAG
        </CustomButton>
      </form>
    </div>
  );
};

export default TagsManagement;
