import React from "react";
import { Header } from "../components";

const styles = {
  screenContainer: {
    padding: "0 20px"
  },
};

const ScreenComponent = ({ children }) => {
  return (
    <>
      <Header />
      <div style={styles.screenContainer}>{children}</div>
    </>
  );
};

export default ScreenComponent;
