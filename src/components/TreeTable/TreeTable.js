import React, { useState } from "react";
import TablePagination from "@material-ui/core/TablePagination";

import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import { cutText, getLastItem } from "../../config/Helpers";
import Colors from "../../constants/Colors";
import MissionPathView from "../../pages/mission/MissionPathView";
import Modal from "../../components/Wrappers/Modal";

const colHeaders = [
  "Id",
  "Fila",
  "Columna",
  "Variedad",
  "Año",
  "Último puntaje",
  "Última imagen",
];

const styles = {
  table: {
    minWidth: 650,
  },
  noData: {
    color: Colors.primary,
    margin: "20px",
  },
  colTitle: {
    color: Colors.primary,
    textAlign: "center",
    fontWeight: "bold",
  },
  imageView: {
    cursor: "pointer",
    color: Colors.primary,
    fontWeight: "bold",
    fontSize: 14,
  },
};

const TreeTable = ({ treesData, setImageToShow, setShowDetails }) => {
  const [viewOnMap, setViewOnMap] = useState();

  const [page, setPage] = useState(1);
  const rowsPerPage = 10;

  const handleChangePage = (event, newPage) => {
    setPage(newPage === page ? page + 1 : newPage === 0 ? 1 : newPage + 1);
  };

  const getMissionPath = (tree) => {
    return {
      latitude: tree.position.coordinates[0],
      longitude: tree.position.coordinates[1],
    };
  };

  const treesPaginated = treesData
    ? treesData.slice((page - 1) * rowsPerPage, page * rowsPerPage)
    : [];

  return (
    <>
      <TableContainer component={Paper}>
        <Table style={styles.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              {colHeaders.map((colName) => (
                <TableCell style={styles.colTitle}>{colName}</TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {treesPaginated ? (
              treesPaginated.map((_tree) => {
                const tree = _tree.treeData.tree;
                const lastImage = getLastItem(tree.images);
                return (
                  <TableRow key={tree.id}>
                    <TableCell align="center">{cutText(tree._id, 6)}</TableCell>
                    <TableCell align="center">{tree.index.row}</TableCell>
                    <TableCell align="center">{tree.index.column}</TableCell>
                    <TableCell align="center">{tree.variety}</TableCell>
                    <TableCell align="center">{tree.year}</TableCell>
                    <TableCell align="center">
                      {tree.lastScore || "- NO DISPONIBLE -"}
                    </TableCell>
                    <TableCell align="center">
                      {lastImage && lastImage.image ? (
                        <h5
                          style={styles.imageView}
                          onClick={() => setImageToShow(lastImage.image.url)}
                        >
                          VER IMAGEN
                        </h5>
                      ) : (
                        "- NO DISPONIBLE -"
                      )}
                    </TableCell>
                    <TableCell align="center">
                      <h5
                        style={styles.imageView}
                        onClick={() => {
                          setViewOnMap(getMissionPath(_tree.treeData.tree));
                        }}
                      >
                        VER EN MAPA
                      </h5>
                    </TableCell>
                    <TableCell align="center">
                      <h5
                        style={styles.imageView}
                        onClick={() => {
                          setShowDetails(_tree.treeData);
                        }}
                      >
                        VER DETALLES
                      </h5>
                    </TableCell>
                  </TableRow>
                );
              })
            ) : (
              <h3 style={styles.noData}>SIN DATOS</h3>
            )}
          </TableBody>
        </Table>
        <TablePagination
          component="div"
          count={treesData ? treesData.length : 0}
          page={page - 1}
          onChangePage={handleChangePage}
          rowsPerPage={10}
          rowsPerPageOptions={[]}
          labelDisplayedRows={({ from, to, count }) =>
            `${from}-${to} de ${count} Árboles`
          }
        />
      </TableContainer>

      {viewOnMap && (
        <Modal
          title="Ver árbol en mapa"
          open={viewOnMap}
          onClose={() => setViewOnMap(false)}
        >
          <MissionPathView path={[viewOnMap]} zoom={16} />
        </Modal>
      )}
    </>
  );
};

export default TreeTable;
