import React from "react";
import Colors from "../../constants/Colors";

const styles = {
  delete: {
    cursor: "pointer",
    fontWeight: "bold",
  },
};

const TagCard = ({ name, color, onDeleteTag }) => {
  return (
      <div style={{margin: "3px",
      border: `3px solid ${color}`,
      background: "white",
      borderRadius: "20px",
      display: "flex",
      justifyContent: "space-between",
      marginBottom: 5,
      padding: 20,
    }}>
        <span>{name}</span>
        <span style={styles.delete} onClick={onDeleteTag}>
          Eliminar
        </span>
      </div>
  );
};

export default TagCard;
