import React, { useState, useEffect } from "react";
import { Route, Redirect } from "react-router-dom";
import CircularProgress from "@material-ui/core/CircularProgress";
import colors from "../constants/Colors";
import { getUser, navigateTo } from "../config/Helpers";

const styles = {
  loadingBackground: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    height: "-webkit-fill-available",
    backgroundColor: "white",
  },
  loadingSpinner: {
    color: colors.primary,
  },
};

const PrivateRoute = ({ component: Component, ...rest }) => {
  const [isLoading, setIsLoading] = useState(true);
  const [validated, setValidated] = useState(false);

  useEffect(() => {
    const user = getUser();
    if (user && user.token) {
      setValidated(true);
      setIsLoading(false);
    } else {
      navigateTo("/logout");
    }
  }, []);

  if (isLoading) {
    return (
      <div style={styles.loadingBackground}>
        <CircularProgress style={styles.loadingSpinner} />
      </div>
    );
  }

  return (
    <Route
      {...rest}
      render={(props) =>
        validated ? <Component {...props} /> : <Redirect to="/login" />
      }
    />
  );
};

export default PrivateRoute;
