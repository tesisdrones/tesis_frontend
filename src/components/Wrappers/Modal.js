import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
} from "@material-ui/core";
import React from "react";
import CustomButton from "./Button";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  Tooltip: {
    margin: "5px",
  },
  Title: {
    color: "white",
    fontWeight: "bold", // not working
    fontSize: "18px",
    background: "rgb(64, 194, 147)",
    border: "2px solid #66fd97",
  },
  modal: {
    borderRadius: "20px",
  },
}));

const Modal = ({
  open = false,
  onClose = () => {},
  title,
  children,
  disableDismiss,
  ...props
}) => {
  const classes = useStyles();
  return (
    <Dialog
      className={classes.modal}
      open={open}
      onClose={onClose}
      {...props}
      disableBackdropClick={disableDismiss}
    >
      <DialogTitle className={classes.Title}>{title}</DialogTitle>
      <DialogContent>{children}</DialogContent>
      <DialogActions>
        <CustomButton variant="contained" onClick={onClose}>
          Cerrar
        </CustomButton>
      </DialogActions>
    </Dialog>
  );
};

export default Modal;
