import React from "react";
import { Button } from "@material-ui/core";
import CircularProgress from "@material-ui/core/CircularProgress";
import colors from "../../constants/Colors";

const styles = {
  btn: {
    backgroundColor: colors.primary,
    border: "2px solid #12e5af",
    height: 30,
    color: colors.white,
    padding: "0px 20px",
    fontFamily: "Poppins, sans-serif",
    fontSize: 14,
  },
  btnDisabled: {
    opacity: 0.5,
    borderRadius: 5,
    height: 30,
    color: colors.white,
    padding: "0px 20px",
    fontFamily: "Poppins, sans-serif",
    fontSize: 14,
  },
};

const CustomButton = ({
  style,
  spinnerSize,
  children,
  disableClick,
  disabled,
  isloading,
  spinnerColor,
  spinnerLabel,
  type,
  ...props
}) => {
  return (
    <Button
      style={
        disabled
          ? { ...styles.btnDisabled, ...style }
          : { ...styles.btn, ...style }
      }
      disabled={disabled || disableClick || isloading}
      type={type || "button"}
      {...props}
    >
      {isloading ? (
        <>
          <small>{spinnerLabel}</small>{" "}
          <CircularProgress
            size={spinnerSize || 10}
            style={{
              color: spinnerColor || "white",
              marginLeft: spinnerLabel ? 10 : 0,
            }}
          />
        </>
      ) : (
        children
      )}
    </Button>
  );
};

export default CustomButton;
