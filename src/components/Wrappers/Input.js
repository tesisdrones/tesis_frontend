import React from "react";
import TextField from "@material-ui/core/TextField";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import Colors from "../../constants/Colors";
import { makeStyles } from "@material-ui/core/styles";

const styles = {
  customLabel: {
    fontSize: 14,
    color: Colors.primary,
  },
};

const useStyles = makeStyles((theme) => ({
  select: {
    width: "100%",
    "&:before": {
      borderColor: Colors.primary,
    },
    "&:after": {
      borderColor: Colors.primary,
    },
    "&:focus": {
      borderRadius: 4,
      borderColor: Colors.primary,
      boxShadow: "0 0 0 0.2rem red",
    },
    "&:hover": {
      borderColor: Colors.primary,
    },
    "&.Mui-focused": {
      borderColor: Colors.white,
      backgroundColor: Colors.white,
    },

    "& label.Mui-focused": {
      color: Colors.primary,
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: Colors.primary,
    },
    "& .MuiOutlinedInput-root": {
      "&:hover fieldset": {
        borderColor: Colors.primary,
      },
      "&.Mui-focused fieldset": {
        borderColor: Colors.primary,
      },
    },
  },
}));

const CustomInput = ({
  children,
  type,
  onChange,
  label,
  customLabel,
  value,
  size,
  style,
  ...props
}) => {
  const classes = useStyles();

  if (type === "select") {
    return (
      <FormControl
        className={classes.select}
        variant="outlined"
        size={size}
        style={style}
      >
        <InputLabel className={classes.select} id={label}>
          {label}
        </InputLabel>
        <Select {...props} onChange={onChange} value={value}>
          {children}
        </Select>
      </FormControl>
    );
  } else if (type === "number") {
    return (
      <TextField
        variant="outlined"
        className={classes.select}
        {...props}
        onChange={onChange}
        label={!customLabel && label}
        type={type}
        defaultValue={value}
        value={value}
        size={size}
        style={style}
      >
        {children}
      </TextField>
    );
  }
  return (
    <>
      {customLabel && <span style={styles.customLabel}>{customLabel}</span>}
      <TextField
        variant="outlined"
        className={classes.select}
        {...props}
        onChange={onChange}
        label={!customLabel && label}
        type={type}
        defaultValue={value}
        value={value}
        size={size}
        style={style}
      >
        {children}
      </TextField>
    </>
  );
};

export default CustomInput;
