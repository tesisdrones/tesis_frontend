import { CircularProgress } from "@material-ui/core";
import React from "react";
import colors from "../../constants/Colors";

const styles = {
  spinner: {
    color: colors.primary,
    display: "block",
    margin: "0 auto",
  },
  message: {
    color: colors.primary,
    display: "block",
    margin: "1vh auto 0",
    textAlign: "center",
  },
};

const Spinner = ({ message, containerStyles, ...props }) => {
  return (
    <div style={containerStyles || {}}>
      <CircularProgress style={styles.spinner} {...props} />
      {message && <span style={styles.message}>{message}</span>}
    </div>
  );
};

export default Spinner;
