import { Switch as MUSwitch } from "@material-ui/core";
import React from "react";

const styles = {
  container: {
    fontWeight: "bold",
    fontSize: 16,
  },
  noMargin: {
    margin: 0,
  },
};

const Switch = ({ labelLeft, labelRight, checked, onChange }) => {
  return (
    <div style={styles.container}>
      {labelLeft}
      <MUSwitch
        color="default"
        checked={checked}
        onChange={onChange}
        name={labelLeft}
      />
      {labelRight}
    </div>
  );
};

export default Switch;
