import React from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import LinearProgress from "@material-ui/core/LinearProgress";
import colors from "../constants/Colors";

const BorderLinearProgress = withStyles((theme) => ({
  root: {
    height: 10,
    borderRadius: 5,
  },
  colorPrimary: {
    backgroundColor:
      theme.palette.grey[theme.palette.type === "light" ? 200 : 700],
  },
  bar: {
    borderRadius: 5,
    backgroundColor: colors.primary,
  },
}))(LinearProgress);

const useStyles = makeStyles({
  root: {
    flexGrow: 1,
  },
});

const ProgressBar = ({ progress }) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <BorderLinearProgress
        variant="determinate"
        value={progress}
        valueBuffer
      />
    </div>
  );
};

export default ProgressBar;
