import React from "react";
import { Card } from "..";
import Chip from "@material-ui/core/Chip";
import { Grid } from "@material-ui/core";

import CustomButton from "../Wrappers/Button";
import Colors from "../../constants/Colors";

const styles = {
  sectionTitle: {
    fontSize: 18,
    color: "rgb(64, 194, 147)",
    fontWeight: "bold",
    marginLeft: "3px",
  },
  title: {
    fontSize: 12,
    color: "rgb(64, 194, 147)",
    fontWeight: "bold",
  },
  result: {
    fontSize: 20,
    display: "block",
  },
  action: {
    cursor: "pointer",
    fontWeight: "bold",
    display: "block",
    fontSize: 10,
  },
  resultContainer: {
    margin: "10px 0",
  },
  card: {
    marginTop: "10px",
    border: "1px solid rgb(64, 194, 147)",
    borderRadius: "10px",
    background: "white",
    color: "black",
  },
  chip: {
    margin: "3px",
  },
  btnContainer: {
    margin: "30px 0 30px auto",
  },
  btn: {
    border: "2px solid #12e5af",
    width: "100%",
    margin: "2px 0",
  },
  image: {
    marginTop: "10px",
    border: "1px solid rgb(64, 194, 147)",
    borderRadius: "10px",
    background: "white",
    color: "black",
    cursor: "zoom-in",
  },
};

const MapScoringCard = ({
  imageFile,
  onZoom,
  score,
  onScore,
  imageDate,
  details,
  onViewMoreDetails,
  year,
  treeVariety,
  tags,
  onShowTraceability,
}) => {
  const ResultValue = ({ title, result, actionTitle, onActionClick }) => (
    <div style={styles.resultContainer}>
      <span style={styles.title}>{title}</span>
      <span style={styles.result}>{result}</span>
      {onActionClick && (
        <small onClick={onActionClick} style={styles.action}>
          {actionTitle}
        </small>
      )}
    </div>
  );

  return (
    <Card
      style={styles.card}
      tooltip="En esta seccion puedes ver la ultima informacion sobre el arbol seleccionado, tambien puedes ampliar la foto haciendole click."
      tooltipStyles={{ color: Colors.primary }}
    >
      <Grid item xs={12}>
        <div>
          <p style={styles.sectionTitle}>Información</p>
        </div>
      </Grid>
      {
        <ResultValue
          result={
            imageFile ? (
              <img
                width="100%"
                src={imageFile}
                alt="imagen"
                style={styles.image}
                onClick={onZoom}
              />
            ) : (
              "s/e"
            )
          }
        />
      }
      <ResultValue title="VARIEDAD" result={treeVariety} />
      <ResultValue title="AÑO DEL ÁRBOL" result={year || "s/e"} />
      <ResultValue title="ESTADO" result={score || "s/c"} />
      <ResultValue title="FECHA DE MUESTRA" result={imageDate || "s/e"} />

      {tags.length > 0 && (
        <ResultValue
          title="TAGS"
          result={
            tags.map((tag) => (
              <Chip
                m="10rem"
                style={{
                  margin: "3px",
                  border: `3px solid ${tag.color}`,
                  background: "white",
                }}
                label={tag.name || "s/e"}
              />
            )) || "s/e"
          }
        />
      )}

      <ResultValue
        title="DETALLES"
        result={details || "s/e"}
        actionTitle="VER MÁS"
        onActionClick={onViewMoreDetails}
      />

      {imageFile && (
        <div style={styles.btnContainer}>
          <CustomButton onClick={onScore} style={styles.btn}>
            Calificar
          </CustomButton>
          <CustomButton onClick={onShowTraceability} style={styles.btn}>
            Trazabilidad
          </CustomButton>
        </div>
      )}
    </Card>
  );
};

export default MapScoringCard;
