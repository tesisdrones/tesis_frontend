import React from "react";
import { Card } from "..";
import Switch from "../Wrappers/Switch";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  switchCard: {
    background: "white",
    border: "4px solid red",
  },
}));

const MapSwitchCard = ({ listViewEnabled, toggleSwitch }) => {
  const classes = useStyles();

  return (
    <Card
      className={classes.switchCard}
      tooltip="Elije entre la vista de Mapa o de Lista para inspeccionar los arboles."
      tooltipStyles={{ marginTop: 5 }}
    >
      {/* <p>VISTA POR</p> */}
      <Switch
        labelLeft="Mapa"
        labelRight="Lista"
        onChange={toggleSwitch}
        checked={listViewEnabled}
      />
    </Card>
  );
};

export default MapSwitchCard;
