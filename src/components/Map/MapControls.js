import React from "react";
import {
  NavigationControl,
  FullscreenControl,
  ScaleControl,
  GeolocateControl,
} from "react-map-gl";

const styles = {
  geolocateStyle: {
    position: "absolute",
    top: 0,
    left: 0,
    padding: "10px",
  },
  fullscreenControlStyle: {
    position: "absolute",
    top: 36,
    left: 0,
    padding: "10px",
  },
  navStyle: {
    position: "absolute",
    top: 72,
    left: 0,
    padding: "10px",
  },
  scaleControlStyle: {
    position: "absolute",
    bottom: 36,
    left: 0,
    padding: "10px",
  },
};

const MapControls = () => {
  return (
    <>
      <div style={styles.geolocateStyle}>
        <GeolocateControl />
      </div>
      <div style={styles.fullscreenControlStyle}>
        <FullscreenControl />
      </div>
      <div style={styles.navStyle}>
        <NavigationControl />
      </div>
      <div style={styles.scaleControlStyle}>
        <ScaleControl />
      </div>
    </>
  );
};

export default MapControls;
