import React, { useState } from "react";
import { Marker } from "react-map-gl";
import { getColorByScore } from "../../config/Helpers";

const styles = {
  pinSelector: {
    fontSize: 50,
    fontWeight: "bold",
    color: "aquamarine",
    position: "absolute",
    bottom: 15,
    right: -8,
    marginRight: "6px",
    marginTop: "2px",
  },
};

const Pins = ({ data, onClick, selectedPinIndex }) => {
  return data.map((tree, index) => (
    <Marker
      key={`marker-${index}`}
      longitude={parseFloat(tree.longitude || tree.long)}
      latitude={parseFloat(tree.latitude || tree.lat)}
    >
      {index === selectedPinIndex && <span style={styles.pinSelector}>↓</span>}
      <span
        style={{
          color: tree.score ? getColorByScore(tree.score) : "white",
          cursor: "pointer",
        }}
        onClick={() => onClick(tree, index)}
      >
        <img
          alt="marker"
          style={{ height: "20px" }}
          src={require(`../../../src/images/tree${tree.score || 0}.svg`)}
          title={`Árbol - ${tree.score || 0}`}
        />
      </span>
    </Marker>
  ));
};
export default Pins;
