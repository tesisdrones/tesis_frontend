import React, { useCallback } from "react";
import { Marker } from "react-map-gl";
import Tree from "../../../src/images/tree5.svg";

const pinStyle = {
  fontSize: 30,
};

const DraggablePin = ({ marker, setMarker }) => {
  // Draggable pin component
  const DraggablePin = () => (
    <span style={pinStyle} role="img" aria-label="tree">
      <img alt="draggable icon" src={Tree} style={{ height: "20px" }} />
    </span>
  );

  const onMarkerDragEnd = useCallback((event) => {
    setMarker({
      longitude: event.lngLat[0],
      latitude: event.lngLat[1],
    });
  }, []);

  return (
    <Marker
      longitude={marker.longitude}
      latitude={marker.latitude}
      offsetTop={-14}
      // offsetLeft={-10}
      draggable
      onDragEnd={onMarkerDragEnd}
    >
      <DraggablePin />
    </Marker>
  );
};

export default DraggablePin;
