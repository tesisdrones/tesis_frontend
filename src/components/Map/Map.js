import React, { useState, useEffect } from "react";
import ReactMapGL from "react-map-gl";
import "mapbox-gl/dist/mapbox-gl.css";
import Pins from "./Pins";
import { defaultViewPort, mapboxApiAccessToken } from "../../config/AppConfig";
import MapControls from "./MapControls";

const Map = ({
  treesData,
  onSetTree,
  disabledControls = false,
  mapConfig,
  onMapClick,
  customPin,
}) => {
  const [viewport, setViewport] = useState({
    latitude: defaultViewPort.latitude,
    longitude: defaultViewPort.longitude,
    zoom: defaultViewPort.zoom,
    maxZoom: defaultViewPort.maxZoom,
    pitch: 90,
    bearing: -50,
  });

  const [selectedPin, setSelectedPin] = useState();

  useEffect(() => {
    if (mapConfig) {
      setViewport({
        ...viewport,
        ...mapConfig,
      });
    }
  }, []);

  const onClick = (e) => {
    onMapClick && onMapClick(e.lngLat);
    setSelectedPin();
    onSetTree && onSetTree();
  };

  const onClickMarker = (selectedTree, selectedIndex) => {
    if (onSetTree) {
      onSetTree(selectedTree.treeData);
      setSelectedPin(selectedIndex);
    }
  };

  return (
    <>
      <div
        style={{
          height: "100%",
          width: "100%",
          border: "5px solid #66fd97",
          borderRadius: "10px",
        }}
      >
        <ReactMapGL
          {...viewport}
          onClick={onClick}
          mapboxApiAccessToken={mapboxApiAccessToken}
          // mapOptions
          mapStyle="mapbox://styles/mapbox/satellite-v9"
          width="100%"
          height="100%"
          onViewportChange={(viewport) => setViewport(viewport)}
        >
          {!disabledControls && <MapControls />}
          <Pins
            data={treesData || []}
            onClick={onClickMarker}
            selectedPinIndex={selectedPin}
          />
          {customPin}
        </ReactMapGL>
      </div>
    </>
  );
};

export default Map;
