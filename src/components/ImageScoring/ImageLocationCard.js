import { Grid } from "@material-ui/core";
import React from "react";
import Colors from "../../constants/Colors";

const styles = {
  container: {
    backgroundColor: Colors.primary,
    padding: "0 20px",
    color: Colors.white,
    fontSize: 16,
    fontWeight: "bold",
    border: `1px solid ${Colors.primary}`,
    borderRadius: 15,
    margin: "10px auto",
    width: "100%",
  },
};

const ImageLocationCard = ({ lat, lon, alt }) => {
  return (
    <div style={styles.container}>
      <Grid container spacing={3}>
        <Grid item xs={6}>
          <p>Latitud</p>
          <p>Longitud</p>
          <p>Altitud</p>
        </Grid>
        <Grid item xs={6}>
          <p>{lat}</p>
          <p>{lon}</p>
          <p>{alt}</p>
        </Grid>
      </Grid>
    </div>
  );
};

export default ImageLocationCard;
