import { Grid } from "@material-ui/core";
import moment from "moment";
import React, { useEffect, useState } from "react";
import { getAllTags, updateScoring } from "../../config/ApiClient";
import { missionDateFormat } from "../../config/AppConfig";
import Modal from "../Wrappers/Modal";
import ImageDetailsCard from "./ImageDetailsCard";
import ImageNewScoringCard from "./ImageNewScoringCard";
import SliderController from "./SliderController";
import Card from "../Card";

const styles = {
  image: {
    maxWidth: "100%",
    maxHeight: "40vw",
    border: "2px solid rgb(64, 194, 147)",
    fontWeight: "bold",
    borderRadius: "10px",
    cursor: "zoom-in",
  },
};

const ImageScoring = ({ show, onClose, images, onUpdate }) => {
  const [missionImages, setMissionImages] = useState();

  const [availableTags, setAvailableTags] = useState([]);
  const [availableTagsObject, setAvailableTagsObject] = useState([]);
  const imagesTotalCount = images && images.length - 1;

  const [currentImageIndex, setCurrentImageIndex] = useState(0);
  const [currentImageScore, setCurrentImageScore] = useState();
  const [currentImageDetails, setCurrentImageDetails] = useState("");
  const [currentImageTags, setCurrentImageTags] = useState([]);

  const [loadingUpdate, setLoadingUpdate] = useState(false);

  const [updateScoreError, setUpdateScoreError] = useState(false);

  useEffect(() => {
    setMissionImages(images);
    fetchTags();
  }, [images]);

  const fetchTags = async () => {
    const tagsResponse = await getAllTags();

    if (tagsResponse.ok) {
      setAvailableTags(tagsResponse.response.map((t) => t.name));
      setAvailableTagsObject(tagsResponse.response.map((t) => t));
    } else {
      console.error(tagsResponse);
    }
  };

  const updateCalifications = async () => {
    if (!currentImageScore) {
      setUpdateScoreError("ⓘ Debe seleccionar nueva calificación para guardar");
      return;
    }

    setLoadingUpdate(true);

    const currentImageTagsId = availableTagsObject
      .filter((element) => currentImageTags.includes(element.name))
      .map((element) => element._id);
    const updateResponse = await updateScoring(
      currentImage._id,
      currentImageScore || 1,
      currentImageDetails,
      currentImageTagsId
      // currentImageTags
    );

    if (updateResponse.ok) {
      // Update missionImage

      if (onUpdate) {
        onUpdate();
        onClose();
      } else {
        missionImages[currentImageIndex].calification = {
          currentImageScore: currentImageScore || 1,
          currentImageDetails,
        };
        setMissionImages([...missionImages]);
      }
    }

    setLoadingUpdate(false);
  };

  const clearInputs = () => {
    setCurrentImageScore(1);
    setCurrentImageDetails("");
  };

  const onNext = () => {
    clearInputs();
    const newIndex =
      currentImageIndex < imagesTotalCount
        ? currentImageIndex + 1
        : currentImageIndex;
    setCurrentImageIndex(newIndex);
  };

  const onBack = () => {
    clearInputs();
    const newIndex = currentImageIndex > 0 ? currentImageIndex - 1 : 0;
    setCurrentImageIndex(newIndex);
  };

  const onSubmitCalification = () => {
    setUpdateScoreError(false);
    updateCalifications();
    onNext();
  };

  const onSetTag = (tag) => {
    setCurrentImageTags(tag);
  };

  const currentImage = missionImages ? missionImages[currentImageIndex] : null;

  return (
    <Modal
      open={show && missionImages}
      onClose={onClose}
      title="Calificar imágenes"
      maxWidth="lg"
    >
      {currentImage && (
        <Grid container spacing={3}>
          <Grid item xs={6}>
            <Card>
              <img
                src={currentImage.url}
                alt={currentImage.url}
                style={styles.image}
              />
              <SliderController
                currentImg={currentImageIndex + 1}
                totalImgCount={imagesTotalCount + 1}
                onNext={onNext}
                onBack={onBack}
              />
            </Card>
          </Grid>
          <Grid item xs={6}>
            <Grid container spacing={1}>
              <ImageNewScoringCard
                isloading={loadingUpdate}
                onSubmit={onSubmitCalification}
                onSetNewScore={setCurrentImageScore}
                onSetNewDetails={setCurrentImageDetails}
                onSetNewTags={onSetTag}
                tagsList={availableTags}
                tagsSelected={currentImageTags}
                showError={updateScoreError}
              />
              <ImageDetailsCard
                lastScore={currentImage.score}
                date={moment(currentImage.lastImageCreationDate).format(
                  "DD/MM/YYYY"
                )}
                lastModifiedDate={moment(
                  currentImage.tree.lastModifiedDate
                ).format("DD/MM/YYYY")}
                row={currentImage.tree.index.row}
                treeNumber={currentImage.tree.index.column}
                variety={currentImage.tree.variety || "-"}
                tags={currentImage.tree.tags || []}
                details={currentImage.details || "-"}
              />
            </Grid>
          </Grid>
        </Grid>
      )}
    </Modal>
  );
};

export default ImageScoring;
