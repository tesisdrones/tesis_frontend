import React, { useState, useEffect } from "react";
import { Grid } from "@material-ui/core";
import { Card } from "..";
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from "react-responsive-carousel";
import Chip from "@material-ui/core/Chip";
import Map from "../Map/Map";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "../Wrappers/Modal";
import { defaultDateFormat, missionDateFormat } from "../../config/AppConfig";
import moment from "moment";

const useStyles = makeStyles((theme) => ({
  sectionTitle: {
    fontWeight: "bold",
    fontSize: "20px",
  },
  infoTitle: {
    fontWeight: "bold",
    marginRight: "10px",
  },
  infoContainer: {
    display: "flex",
  },
  infoValue: {
    color: "black",
  },
  carousel: {
    paddingBottom: 10,
  },
}));

const ImageTraceabilityCard = ({ data, onCloseModal, showModal = false }) => {
  const {
    images = [],
    variety = "",
    tags = [],
    lastModifyDate = "",
    position,
    lastScore,
  } = data || {};

  const tree = {
    treeData: {},
    latitude: position?.coordinates[0],
    longitude: position?.coordinates[1],
    score: lastScore,
  };

  const classes = useStyles();
  const trees = [];
  trees.push(tree);

  const onCloseHandler = () => {
    onCloseModal();
  };

  const [sortedImages, setSortedImages] = useState();

  useEffect(() => {
    const ord = images.sort((a, b) =>
      a.image.creationDate > b.image.creationDate
        ? 1
        : a.image.creationDate === b.image.creationDate
        ? a.image.creationDate > b.image.creationDate
          ? 1
          : -1
        : -1
    );
    setSortedImages(ord);
  }, [images]);

  return (
    <>
      {sortedImages && (
        <Modal
          open={showModal}
          onClose={onCloseHandler}
          title="Trazabilidad de Arbol"
          maxWidth="lg"
        >
          <Carousel
            centerMode
            swipeable
            emulateTouch
            centerSlidePercentage={66}
            showArrows
            useKeyboardArrows
            autoPlay={false}
            showStatus={false}
            stopOnHovers
            className={classes.carousel}
          >
            {sortedImages.map(
              (image) =>
                image.image && (
                  <Card
                    key={image}
                    style={{
                      margin: "10px 10px 40px",
                      padding: 10,
                      cursor: "grab",
                      border: "1px solid #66fd97",
                      borderRadius: 10,
                    }}
                  >
                    <Grid xs={12} spacing={1} container>
                      <Grid
                        className="legend"
                        style={{ display: "flex" }}
                        xs={12}
                        Item
                      >
                        <Grid
                          item
                          xs={1}
                          style={{
                            width: "20px",
                            border: "5px solid #66fd97",
                            fontWeight: "bold",
                            borderRadius: "15px",
                            minWidth: "58px",
                            fontSize: "35px",
                          }}
                        >
                          {" "}
                          {image.image.score || "0"}{" "}
                        </Grid>
                        <Grid item xs={10}>
                          {` ${
                            image.image.details ||
                            "No hay ningún detalle para este árbol."
                          }`}
                        </Grid>
                        <Grid
                          item
                          xs={1}
                          style={{
                            color: "#66fd97",
                            fontWeight: "bold",
                            alignSelf: "flex-end",
                            marginLeft: "5px",
                            marginRight: "5px",
                          }}
                        >
                          {" "}
                          {moment(image.image.creationDate).format(
                            defaultDateFormat
                          ) || "-"}{" "}
                        </Grid>
                      </Grid>
                    </Grid>
                    <Grid container xs={12}>
                      <Grid item xs={12}>
                        <img
                          src={image.image.url}
                          style={{
                            border: "1px solid #66fd97",
                            borderRadius: "10px",
                            maxHeight: "443px",
                          }}
                          width="100%"
                          height="100%"
                          alt="img-preview"
                        />
                      </Grid>
                    </Grid>
                  </Card>
                )
            )}
          </Carousel>
          <Grid container xs={12} spacing={2}>
            <Grid item xs={6}>
              <Card primary>
                <p className={classes.sectionTitle}> Información</p>
                <div className={classes.infoContainer}>
                  <p className={classes.infoTitle}>Variedad:</p>
                  <p className={classes.infoValue}>{variety}</p>
                </div>
                <div className={classes.infoTinfoContaineritle}>
                  <p className={classes.infoTitle}>Tags:</p>
                  <p className={classes.infoValue}>
                    {tags.map((tag) => (
                      <Chip
                        label={tag.name}
                        m="10rem"
                        style={{
                          margin: "3px",
                          border: `3px solid ${tag.color}`,
                          background: "white",
                        }}
                      />
                    ))}
                  </p>
                </div>
                <div className={classes.infoContainer}>
                  <p className={classes.infoTitle}>Última Modificacion: </p>
                  <p className={classes.infoValue}>
                    {moment(lastModifyDate, missionDateFormat).format(
                      "DD/MM/YYYY"
                    ) || "-"}
                  </p>
                </div>
              </Card>
            </Grid>
            <Grid item xs={6}>
              <Card>
                <p className={classes.sectionTitle}>Ubicación</p>
                <p style={{ height: "200px" }}>
                  <Map
                    treesData={trees}
                    onSetTree={{}}
                    disabledControls={true}
                    mapConfig={{
                      zoom: 18,
                      pitch: 0,
                      bearing: 0,
                      latitude: tree.latitude,
                      longitude: tree.longitude,
                    }}
                  />
                </p>
              </Card>
            </Grid>
          </Grid>
        </Modal>
      )}
    </>
  );
};

export default ImageTraceabilityCard;
