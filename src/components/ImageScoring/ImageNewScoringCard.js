import React from "react";
import { Checkbox, Grid, ListItemText, MenuItem } from "@material-ui/core";
import { imageRates } from "../../config/AppConfig";
import Colors from "../../constants/Colors";
import CustomButton from "../Wrappers/Button";
import CustomInput from "../Wrappers/Input";
import Spinner from "../Wrappers/Spinner";

const styles = {
  container: {
    padding: "5px 20px",
    color: Colors.primary,
    backgroundColor: Colors.white,
    border: `1px solid ${Colors.green}`,
    borderRadius: "10px",
    // padding: "10px 20px",
    boxShadow: "0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)",
    // marginTop: "10px",
    marginBottom: "10px",
    width: "100%",
  },
  title: {
    display: "flex",
    justifyContent: "center",
    flexDirection: "column",
    fontWeight: "bold",
  },
  row: {
    margin: "15px 0",
  },
  error: {
    fontWeight: "initial",
    color: `${Colors.error}`,
    display: "block",
  },
};

const ImageNewScoringCard = ({
  onSetNewScore,
  onSetNewDetails,
  onSetNewTags,
  tagsList,
  tagsSelected,
  onSubmit,
  isloading,
  showError,
}) => {
  return (
    <div style={styles.container}>
      {isloading ? (
        <Spinner />
      ) : (
        <>
          <Grid container style={styles.row}>
            <Grid item xs={6} style={styles.title}>
              Calificación actual
            </Grid>
            <Grid item xs={6}>
              <CustomInput
                type="select"
                onChange={(e) => onSetNewScore(e.target.value)}
                size="small"
                label="Calificación"
              >
                {imageRates.map((c) => (
                  <MenuItem key={`calif-${c}`} value={c}>
                    {c}
                  </MenuItem>
                ))}
              </CustomInput>
            </Grid>
          </Grid>
          {tagsList && tagsList.length > 0 && (
            <Grid container style={styles.row}>
              <Grid item xs={6} style={styles.title}>
                Tags
              </Grid>
              <Grid item xs={6}>
                <CustomInput
                  type="select"
                  defaultValue={tagsSelected || []}
                  onChange={(e) => onSetNewTags(e.target.value)}
                  size="small"
                  label="Tag"
                  multiple
                  renderValue={(selected) => selected.join(", ")}
                >
                  {tagsList.map((t) => (
                    <MenuItem key={`tag-${t}`} value={t}>
                      <Checkbox
                        style={{ color: Colors.primary }}
                        checked={tagsSelected.indexOf(t) > -1}
                      />
                      <ListItemText primary={t} />
                    </MenuItem>
                  ))}
                </CustomInput>
              </Grid>
            </Grid>
          )}

          <Grid container style={styles.row}>
            <Grid item xs={6} style={styles.title}>
              Detalles
            </Grid>
            <Grid item xs={6}>
              <CustomInput
                multiline
                onChange={(e) => onSetNewDetails(e.target.value)}
                rows={2}
                size="small"
                label="Detalles"
                placeholder="Detalles del árbol..."
              />
            </Grid>
          </Grid>
        </>
      )}
      <CustomButton
        size="small"
        style={{ float: "right", marginBottom: 10 }}
        variant="contained"
        onClick={onSubmit}
        isloading={isloading}
      >
        GUARDAR Y CONTINUAR
      </CustomButton>
      {showError && <span style={styles.error}>{showError}</span>}
    </div>
  );
};

export default ImageNewScoringCard;
