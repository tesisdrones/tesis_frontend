import { Grid } from "@material-ui/core";
import React from "react";
import Colors from "../../constants/Colors";
import Chip from "@material-ui/core/Chip";

const styles = {
  container: {
    border: `1px solid ${Colors.powerGreen}`,
    boxShadow: "0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)",
    padding: "10px 20px",
    // color: `${Colors.green}`,
    borderRadius: "10px",
    // marginTop: "10px",
    marginBottom: "10px",
    background: `${Colors.white}`,
    width: "100%",
  },
  attributeTitle: {
    fontWeight: "bold",
    color: `${Colors.green}`,
  },
};

const ImageDetailsCard = ({
  lastScore,
  date,
  lastModifiedDate,
  row,
  treeNumber,
  variety,
  tags,
  details,
}) => {
  return (
    <div style={styles.container}>
      <Grid container spacing={3}>
        <Grid item xs={6}>
          <p style={styles.attributeTitle}>Ultima Calificación</p>
          <p style={styles.attributeTitle}>Fecha de muestra</p>
          <p style={styles.attributeTitle}>Fecha de ult. calificación</p>
          <p style={styles.attributeTitle}>Fila</p>
          <p style={styles.attributeTitle}>Árbol</p>
          <p style={styles.attributeTitle}>Variedad</p>
          <p style={styles.attributeTitle}>Tags</p>
          <p style={styles.attributeTitle}>Detalles</p>
        </Grid>
        <Grid item xs={6}>
          <p>{lastScore || "S/D"}</p>
          <p>{date}</p>
          <p>{lastModifiedDate}</p>
          <p>{row}</p>
          <p>{treeNumber}</p>
          <p>{variety}</p>
          {/* <p>{tags.length > 0 ? tags.join(", ") : "s/e"}</p> */}
          <p>
            {tags.map((tag) => (
              <Chip
                label={tag.name}
                m="10rem"
                style={{
                  margin: "3px",
                  border: `3px solid ${tag.color}`,
                  background: "white",
                }}
              />
            ))}
          </p>

          <p>{details}</p>
        </Grid>
      </Grid>
    </div>
  );
};

export default ImageDetailsCard;
