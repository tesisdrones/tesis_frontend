import React from "react";
import { Grid } from "@material-ui/core";
import { Card } from "..";

const ImageTraceabilityCard = ({ images }) => {
  return (
    <>
      {images.map((image) => (
        <>
          {image.image && (
            <Card style={{ marginBottom: 10 }}>
              <Grid container spacing={2}>
                <Grid item md={6}>
                  <img src={image.image.url} width="100%" alt="img-preview" />
                </Grid>
                <Grid item md={6}>
                  <Grid container spacing={2}>
                    <Grid item md={6}>
                      <p>Calificación:</p>
                      <p>Detalles:</p>
                    </Grid>
                    <Grid item md={6}>
                      <p>{image.image.score || "-"}</p>
                      <p>{image.image.details || "-"}</p>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Card>
          )}
        </>
      ))}
    </>
  );
};

export default ImageTraceabilityCard;
