import React from "react";
import Colors from "../../constants/Colors";

const styles = {
  container: {
    display: "flex",
    justifyContent: "space-between",
    marginTop: 10,
  },
  slideController: {
    width: 100,
    position: "relative",
    top: -15,
    right: 15,
    color: Colors.primary,
    display: "flex",
    justifyContent: "space-between",
  },
  countContainer: {
    color: Colors.primary,
    fontWeight: "bold",
    fontSize: 16,
    float: "left",
  },
  control: {
    fontSize: 35,
    fontWeight: "bold",
    cursor: "pointer",
  },
};

const SliderController = ({ onBack, onNext, currentImg, totalImgCount }) => {
  return (
    <div style={styles.container}>
      <div
        style={styles.countContainer}
      >{`Imagen ${currentImg} de ${totalImgCount}`}</div>
      <div style={styles.slideController}>
        <span onClick={onBack} style={styles.control}>{`<`}</span>
        <span onClick={onNext} style={styles.control}>{`>`}</span>
      </div>
    </div>
  );
};

export default SliderController;
