import React, { useState } from "react";
import ImageUploader from "react-images-upload";
import EXIF from "exif-js";
import _ from "lodash";
import Button from "./Wrappers/Button";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCloudUploadAlt } from "@fortawesome/free-solid-svg-icons";
import { imageMaxFileSize } from "../config/AppConfig";
import { uploadImage } from "../config/ApiClient";
import ProgressBar from "./ProgressBar";
import colors from "../constants/Colors";
import {
  blobToFile,
  getDecimalCoordinates,
  minifyImg,
} from "../config/Helpers";

const styles = {
  container: {
    margin: "auto",
    maxWidth: 700,
    display: "block",
    padding: 20,
    textAlign: "center",
    color: colors.primary,
  },
  btnUpload: {
    backgroundColor: colors.primary,
    borderRadius: 5,
    padding: "10px 20px",
    fontSize: 16,
  },
  btnSubmit: {
    fontSize: 18,
    padding: "20px 40px",
    marginTop: 20,
  },
  error: {
    color: "red",
    textAlign: "center",
  },
  uploadIcon: {
    position: "relative",
    bottom: 2,
    left: 10,
  },
  imageErrorContainer: {
    marginTop: 20,
  },
  uploadingHelper: {
    display: "block",
    marginTop: 20,
  },
};

const UploadImages = ({ missionId }) => {
  const [uploading, setUploading] = useState(false);
  const [toUpload, setToUpload] = useState([]);

  const [errorMessage, setErrorMessage] = useState();
  const [uploadProgress, setUploadProgress] = useState(0);

  const [failedImages, setFailedImages] = useState([]);

  const [processingImages, setProcessingImages] = useState(false);

  const onDrop = async (pictureFiles, pictureDataURLs) => {
    setProcessingImages(true);
    const compressedImages = await compressImg(pictureFiles);
    setProcessingImages(false);
    setToUpload(compressedImages);
  };

  const compressImg = async (pictureFiles) => {
    const items = Promise.all(
      pictureFiles.map(async (pf) => {
        try {
          const coordinates = await getImageMetadata(pf);
          const data = await minifyImg(pf);
          const newFile = await blobToFile(data, pf.name);
          const newObj = {
            file: newFile,
            lat: coordinates.lat,
            lon: coordinates.lon,
          };
          return newObj;
        } catch (e) {
          return setErrorMessage(e);
        }
      })
    );
    return items;
  };

  const getImageMetadata = async (file) => {
    const coord = new Promise((resolve, reject) => {
      EXIF.getData(file, function () {
        const lat = EXIF.getTag(this, "GPSLatitude");
        const long = EXIF.getTag(this, "GPSLongitude");
        const latRef = EXIF.getTag(this, "GPSLatitudeRef");
        const longRef = EXIF.getTag(this, "GPSLongitudeRef");

        if (lat && long) {
          resolve({
            lat: getDecimalCoordinates(lat[0], lat[1], lat[2], latRef),
            lon: getDecimalCoordinates(long[0], long[1], long[2], longRef),
          });
        } else {
          reject("Las imágenes seleccionadas no tienen coordenadas");
        }
      });
    });
    return coord;
  };

  const submitImages = async () => {
    setErrorMessage();

    if (!toUpload || _.isEmpty(toUpload)) {
      return setErrorMessage("No se seleccionaron imágenes");
    }

    if (!missionId) {
      return setErrorMessage("No se reconoce el id de mission");
    }

    let uploadProgressCount = 0;

    toUpload.map(async (img) => {
      setUploading(true);
      const formData = new FormData();
      formData.append("image", img.file);
      formData.append("latitude", img.lat);
      formData.append("longitude", img.lon);

      const uploadResponse = await uploadImage(missionId, formData);
      uploadProgressCount++;

      if (!uploadResponse.ok) {
        setFailedImages([...failedImages, img["file"].name]);
      }

      const currentProgress = (uploadProgressCount * 100) / toUpload.length;
      setUploadProgress(currentProgress);
      currentProgress === 100 && setUploading(false);
    });
  };

  const uploadingImages = uploadProgress ? uploadProgress < 100 : null;

  return (
    <>
      <div style={styles.container}>
        {uploadProgress === 100 ? (
          <>
            <h4 style={{ marginTop: 10 }}>Imágenes subidas correctamente</h4>
          </>
        ) : (
          <>
            <h2>SELECCIONE LAS IMÁGENES DESDE EL EXPLORADOR</h2>
            <ImageUploader
              withIcon={true}
              buttonText="SELECCIONAR IMÁGENES"
              buttonStyles={styles.btnUpload}
              onChange={onDrop}
              label="Tamaño máx. 25mb con formato jpg, jpeg o png"
              imgExtension={[".jpg", ".png", ".jpeg"]}
              maxFileSize={imageMaxFileSize}
            />
            {toUpload.length > 0 && (
              <h3>{`${toUpload.length} imágenes seleccionadas`}</h3>
            )}
            <ProgressBar progress={uploadProgress} />
            <Button
              isloading={uploading || processingImages}
              style={styles.btnSubmit}
              disableClick={errorMessage || !toUpload.length}
              onClick={submitImages}
              spinnerLabel={processingImages && "Comprimiendo"}
            >
              SUBIR IMÁGENES{" "}
              <FontAwesomeIcon
                style={styles.uploadIcon}
                icon={faCloudUploadAlt}
              />
            </Button>
            {uploadingImages && (
              <small style={styles.uploadingHelper}>
                Subiendo imágenes, por favor no cierres este diálogo
              </small>
            )}
          </>
        )}
        {errorMessage && <p style={styles.error}>{errorMessage}</p>}

        <div style={styles.imageErrorContainer}>
          {failedImages.map((failedImage) => (
            <p
              style={styles.error}
            >{`La imagen ${failedImage} no pudo ser asignada con algún árbol en el sistema`}</p>
          ))}
        </div>
      </div>
    </>
  );
};

export default UploadImages;
