import React from "react";
import { getUser, navigateTo } from "../config/Helpers";
import { MenuOption } from "../components";
import { useLocation } from "react-router-dom";
import routes from "../config/Routes";
import colors from "../constants/Colors";
import { Container, Nav, Navbar, NavDropdown } from "react-bootstrap";
import "../assets/Header.css";
import LocalOffer from "@material-ui/icons/AccountCircle";
import { androidAppDownloadLink } from "../config/AppConfig";

const LogoWhite = require("../images/logo.svg");

const styles = {
  header: {
    backgroundColor: colors.primary,
    color: "white",
    fontSize: 20,
    border: `2px solid ${colors.powerGreen}`,
    borderBottomRightRadius: 20,
    borderBottomLeftRadius: 20,
    fontWeight: "bold",
    display: "block",
    boxShadow: "0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)",
  },
  title: {
    fontSize: 25,
    fontWeight: "bold",
  },
  alignEnd: {
    display: "flex",
    alignItems: "center",
    justifyContent: "end",
  },
};

const Header = () => {
  const location = useLocation();

  return (
    <Navbar fixed="top" variant="light" style={styles.header} expand="lg">
      <Container style={{ width: "100%", maxWidth: "none", paddingRight: 80 }}>
        <Navbar.Brand href="/">
          <img
            src={LogoWhite}
            style={{ transform: "scale(2.25)", marginLeft: "40%" }}
            width="100"
            alt="logo_white"
          />
        </Navbar.Brand>
        <Navbar.Toggle />
        <Navbar.Collapse>
          <Nav className="ml-auto">
            {routes.map(
              (r, i) =>
                !r.hideInMenu && (
                  <MenuOption
                    key={`header-${i}`}
                    onClick={() => navigateTo(r.path)}
                    selected={location.pathname === r.path}
                    title={r.title}
                    icon={r.icon}
                  />
                )
            )}
          </Nav>

          {/* <Tooltip> */}
          <LocalOffer
            fontSize="default"
            title="Gestiona tu cuenta, etiquetas o simplemente puedes salir de tu cuenta aqui."
          ></LocalOffer>
          {/* </Tooltip> */}
          <NavDropdown title={getUser().user.name} id="navbarScrollingDropdown">
            <NavDropdown.Item href="/trees-management">
              Gestionar árboles
            </NavDropdown.Item>

            <NavDropdown.Item href="/password-update">
              Cambiar contraseña
            </NavDropdown.Item>
            <NavDropdown.Divider />
            <NavDropdown.Item href={androidAppDownloadLink}>
              Descargar app <i className="fa fa-android" aria-hidden="true"></i>
            </NavDropdown.Item>
            <NavDropdown.Divider />
            <NavDropdown.Item href="/logout">Salir</NavDropdown.Item>
          </NavDropdown>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

export default Header;
